const utility = require('../utility/utilities');
const db = require('../models');

exports.sendEmail = async(req, res) => {

  let result = undefined;
  console.log('exports.sendEmail');
  try{
    result = await utility.sendEmailData(
        req.id,
        req.body.toEmail,
        req.body.subject,
        req.body.templateName,
        req.body.paramsEmail,
        false
    );
  } catch (e) {
    result = e;
  }

  res.status(200).send({
    code: 'D200',
    result: result
  });
};

exports.getVersion = async(req, res) => {
  console.log('log de prueba');
  res.status(200).send({
    code: 'D200',
    version: process.env.VERSION
  });
};

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

exports.sendEmailByCategory = async(req, res) => {
  let result = undefined;
  console.log('exports.sendEmailByCategory');

  const category = req.body.category;
  let query = null
  let emails = [];

  switch (category) {
    case 'active-pools':
      query = 'select distinct u.email from q_user u left join q_user_pools qup on u.rowid = qup.fk_q_user left join q_pools qp on qup.fk_q_pools = qp.rowid where qp.status = 1';
      break;
    case 'all-pools':
      query = 'select distinct u.email from q_user u left join q_user_pools qup on u.rowid = qup.fk_q_user';
      break;
    case 'active-users':
      query = 'select u.email from q_user u where u.status = 1';
      break;
    case 'all-users':
      query = 'select u.email from q_user u';
      break;
  }

  if (query !== null) {
    const result = await db.sequelize.query(query);
    emails = result[0].map(x => x.email);
  } else {
    if (category === 'manual-selection') {
      emails = req.body.manualSelection;
    } else {
      emails = process.env.DEFAULT_EMAIL_SEND.split(',');
    }
  }

  let emailsValidated = [];

  emails.forEach(email => {
    if (validateEmail(email)) {
      emailsValidated.push(email);
    }
  })

  console.log(emailsValidated);


  try{
    result = await utility.sendEmailDataWithoutTemplate(
        req.id,
        emailsValidated,
        req.body.subject,
        req.body.message,
        false
    );
  } catch (e) {
    result = e;
  }

  res.status(200).send({
    code: 'D200',
    result: result
  });
};
