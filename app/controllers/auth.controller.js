const db = require('../models');
const config = require('../config/auth.config');
const User = db.q_user;
const Session = db.Session;
var jwt = require('jsonwebtoken');
const Rijndael = require('rijndael-js');
const Op = db.Sequelize.Op;
const uuid = require('uuid');
const utility = require('../utility/utilities');


exports.signup = async (req, res) => {

  let user = await User.findOne({
    where: {
      username: req.body.username,
    },
  });

  if (user) {
    return res.status(200).send({ code: 'A404', message: 'Usuario ya existe', hasError: true, });
  }

  const key = process.env.CIPHER_KEY;

  const original = req.body.password;
  const iv = process.env.CIPHER_IV;
  const cipher = new Rijndael(key, process.env.CIPHER_MODE);
  const ciphertext = Buffer.from(cipher.encrypt(original, 256, iv));
  req.body.password = ciphertext.toString('base64');
  // req.body.status_code = uuid.v4();

  req.body.status = 1;

  try {
    const userData = await User.create(req.body);

    // try{
    //  await utility.sendEmailData(
    //       0,
    //       req.body.email,
    //       'Registro de usuario',
    //       'activateAccount',
    //       {
    //         urlFrontend: process.env.URL_FRONTEND,
    //         code: req.body.status_code
    //       },
    //       false
    //   );
    // } catch (e) {
    //  console.log(e);
    // }

    res.status(200).send({
      code: 'A200',
      hasError: false,
    });
  } catch (e) {
    res.status(200).send({
      code: 'D500',
      message: e.message || 'Error creando el usuario',
      hasError: true,
    });
  }
};

exports.login = async (req, res) => {
  let user = await User.findOne({
    where: {
      email: req.body.username,
    },
  });

  if (!user) {
    return res.status(200).send({ code: 'A404', message: 'Usuario no existe',hasError: true, });
  }

  const key = process.env.CIPHER_KEY;

  const original = req.body.password;
  const iv = process.env.CIPHER_IV;
  const cipher = new Rijndael(key, process.env.CIPHER_MODE);
  const ciphertext = Buffer.from(cipher.encrypt(original, 256, iv));
  const cipherTextString = ciphertext.toString('base64');

  let passwordIsValid = cipherTextString === user.password;

  if (!passwordIsValid) {
    return res.status(200).send({
      accessToken: null,
      code: 'A401',
      message: 'Clave no coincide',
      hasError: true,
    });
  }

  if (user.status === 0) {
    return res.status(200).send({
      accessToken: null,
      code: 'A401',
      message: 'El usuario esta deshabilitado o no tiene permisos para ingresar al sistema',
      hasError: true,
    });
  }

  let session = await Session.findOne({
    where: {
      userId: user.rowid,
      status: { [Op.eq]: 'ACTIVE' },
    },
  });

  if (session) {
    session.dataValues.status = 'EXPIRED BY OTHER LOGIN';

    let sessionUpdated = await Session.update(session.dataValues, {
      where: { id: session.dataValues.id },
    });
  }

  let token = jwt.sign({ id: user.rowid }, config.secret, {
    expiresIn: Number(process.env.EXPIRATION_TOKEN),
  });

  var newDateObj = new Date(
    new Date().getTime() + Number(process.env.EXPIRATION_TOKEN) * 1000
  );

  let dataSession = await Session.create({
    token: token,
    userId: user.rowid,
    expires: newDateObj,
    status: 'ACTIVE',
  });

  user.dataValues.date_Access = Date.now();

  const dataUpdate = await User.update(user.dataValues, {
    where: { rowid: user.dataValues.rowid },
  });

  res.status(200).send({
    id: user.rowid,
    username: user.username,
    email: user.email,
    name: user.firstName,
    lastName: user.lastName,
    img: user.img,
    createDate: user.date_Create,
    amount: user.amount,
    coins: user.coins,
    accessToken: token,
    img: user.img,
    code: 'A200',
    hasError: false,
    isAdmin: user.type === 0,
  });
};

exports.logout = async (req, res) => {
  let session = await Session.findOne({
    where: {
      token: req.query.token,
    },
  });

  if (session) {
    let sessiohnDelete = await Session.destroy({
      where: { token: { [Op.eq]: req.query.token } },
    });

    res.status(200).send({
      code: 'A200',
      hasError: false,
    });
  } else {
    res.status(200).send({
      code: 'D500',
      message: e.message || 'Error eliminando la sesion',
      hasError: true,
    });
  }


};


exports.verifyUsername = async (req, res) => {
  let user = await User.findOne({
    where: {
      username: req.query.username,
    },
  });

  res.status(200).send({
    code: 'A200',
    exist: !!user,
    hasError: false,
  });
};

exports.verifyEmail = async (req, res) => {
  let user = await User.findOne({
    where: {
      email: req.query.email,
    },
  });

  res.status(200).send({
    code: 'A200',
    exist: !!user,
    hasError: false,
  });
};

exports.activateAccount = async (req, res) => {
  let user = await User.findOne({
    where: {
      status_code: req.body.status_code,
    },
  });

  if (user == null) {
    return res.status(200).send({ code: 'A404', message: 'Usuario no encontrado', hasError: true, });
  }

  user.dataValues.status_code = null;
  user.dataValues.status = 1;

  const dataUpdate = await User.update(user.dataValues, {
    where: { rowid: user.dataValues.rowid },
  });

  res.status(200).send({
    code: 'A200',
    hasError: false,
  });
};

exports.recoverPasswordInit = async (req, res) => {
  let user = await User.findOne({
    where: {
      email: req.body.email,
    },
  });

  if (user == null) {
    return res.status(200).send({ code: 'A404', message: 'Usuario no encontrado', hasError: true, });
  }

  user.dataValues.status_code = uuid.v4();
  // user.dataValues.status = 0;

  const dataUpdate = await User.update(user.dataValues, {
    where: { rowid: user.dataValues.rowid },
  });

  try {
    await utility.sendEmailData(
        0,
        req.body.email,
        'Recuperar clave',
        'recoverPasswordInit',
        {
          urlFrontend: process.env.URL_FRONTEND,
          code: user.dataValues.status_code
        },
        false
    );
  } catch (e) {
    console.log(e);
  }

  res.status(200).send({
    code: 'A200',
    hasError: false,
  });
};

exports.recoverPasswordEnd = async (req, res) => {
  let user = await User.findOne({
    where: {
      status_code: { [Op.eq]: req.body.code }
    },
  });

  if (user == null) {
    return res.status(200).send({ code: 'A404', message: 'Usuario no encontrado', hasError: true, });
  }

  user.dataValues.status_code = null;
  user.dataValues.status = 1;

  const key = process.env.CIPHER_KEY;

  const original = req.body.password;
  const iv = process.env.CIPHER_IV;
  const cipher = new Rijndael(key, process.env.CIPHER_MODE);
  const ciphertext = Buffer.from(cipher.encrypt(original, 256, iv));
  user.dataValues.password = ciphertext.toString('base64');


  const dataUpdate = await User.update(user.dataValues, {
    where: { rowid: user.dataValues.rowid },
  });

  res.status(200).send({
    code: 'A200',
    hasError: false,
  });
};
