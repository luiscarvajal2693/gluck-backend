const cron = require("node-cron");
const db = require('../models');
const Session = db.Session;
const Op = db.Sequelize.Op;

// sending emails at periodic intervals
exports.startCronJob = async () =>{

    cron.schedule(process.env.EXPIRATION_CRON_TIME, async () => {
        console.log("---------------------");
        console.log("Running Cron Job");

        let sessions = await Session.findAll({
            where: {
                status: { [Op.eq]: 'ACTIVE' }
            }
        });

        sessions.forEach(x => {
            if (new Date() > x.dataValues.expires) {
                console.log(x);
                x.dataValues.status = 'EXPIRED BY CRON'

                Session.update(
                    x.dataValues, {where: {id: x.dataValues.id}}
                ).then((data) => {
                    console.log("Session Expired!");
                    console.log(data);
                })
                    .catch((err) => {
                        console.log(err);
                    });
            }
        });
    });
}
