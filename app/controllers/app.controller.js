const db = require('../models');
const Menu = db.menus;
const Op = db.Sequelize.Op;
const CsvParser = require('json2csv').Parser;
const Rijndael = require('rijndael-js');
const uuid = require('uuid');
const utility = require('../utility/utilities');

function getAssociations(model, includes) {
  let result = [];
  includes.forEach((include) => {
    result.push({
      model: db[db[model].associations[include].target.tableName],
      as: db[model].associations[include].as,
    });
    // if (include === 'sport') {
    //   result.push(
    //   [
    //     db.sequelize.literal(`(
    //                 SELECT *
    //                 FROM q_sport AS sport
    //                 WHERE
    //                     q_sport.rowid = q_team.fk_sport
    //             )`),
    //     'q_sport'
    //   ] );
    // }
  });

  return result;
}

exports.get = async (req, res) => {
  let condition = null;
  let model = req.query.model;
  let associations = req.query.include
    ? getAssociations(model, JSON.parse(req.query.include))
    : null;

  try {
    let data = await db[model].findAll({
      where: condition,
      order: [['rowid']],
      include: associations,
    });

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message: err.message || 'Some error occurred while retrieving ' + model,
    });
  }
};

exports.getOne = async (req, res) => {
  let model = req.query.model;
  let associations = req.query.include
    ? getAssociations(model, JSON.parse(req.query.include))
    : null;

  try {
    let data = await db[model].findOne({
      where: { rowid: { [Op.eq]: req.query.id } },
      include: associations,
    });

    if (data !== null) {
      if (model === 'q_user') {
        const key = process.env.CIPHER_KEY;

        const iv = process.env.CIPHER_IV;
        const cipher = new Rijndael(key, process.env.CIPHER_MODE);

        let cipherText = data.dataValues.password;

        let plaintext = Buffer.from(cipher.decrypt(new Buffer(cipherText, 'base64'), 256, iv)).toString();
        data.dataValues.password = plaintext.replace(/[^\w\s]/g, '');
      }
    }

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
        err.message ||
        `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

exports.filter = async (req, res) => {
  let model = req.query.model;
  let conditions = {};
  let orders = [['rowid', 'DESC']];
  let associations = req.query.include
    ? getAssociations(model, JSON.parse(req.query.include))
    : null;

  if (req.query.filters !== undefined) {
    req.query.filters.forEach((x) => {
      x = JSON.parse(x);
      if (db[model].rawAttributes[x.id].type.toString() === 'BOOLEAN') {
        conditions[x.id] = x.value;
      } else if (db[model].rawAttributes[x.id].type.toString() === 'INTEGER') {
        conditions[x.id] = {
          // [db.getOperator(db[model].rawAttributes[x.id].type.toString(), Op)]: db.getValue(
          //   db[model].rawAttributes[x.id].type.toString(),
          //   x.value
          // ),
          [Op.eq]: x.value,
        };
      } else {
        conditions[x.id] = {
          [db.getOperator(db[model].rawAttributes[x.id].type.toString(), Op)]:
            db.getValue(db[model].rawAttributes[x.id].type.toString(), x.value),
        };
      }
    });
  }

  // order
  if (req.query.sort !== undefined && req.query.sort.length > 0) {
    req.query.sort[0] = JSON.parse(req.query.sort[0]);
    orders = [[req.query.sort[0].id, req.query.sort[0].desc ? 'DESC' : 'ASC']];
  }

  try {
    if (req.query.paginate) {
      let limit = req.query.itemsForPage;
      let offset = 0 + (req.query.page - 1) * limit;

      let dataCount = await db[model].findAndCountAll({
        where: conditions,
      });

      let data = await db[model].findAll({
        where: conditions,
        offset: offset,
        limit: limit,
        order: orders,
        include: associations,
      });

      res.send({
        data: data,
        code: 'D200',
        totalPages: Math.ceil(dataCount.count / limit),
      });
    } else {
      let data = await db[model].findAll({
        where: conditions,
        order: orders,
      });

      res.send({
        data: data,
        code: 'D200',
      });
    }
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message: err.message || 'Some error occurred while retrieving menu.',
    });
  }
};

exports.download = async (req, res) => {
  let model = req.query.model;
  let conditions = {};
  let orders = [['rowid', 'DESC']];

  if (req.query.filters !== undefined) {
    req.query.filters.forEach((x) => {
      x = JSON.parse(x);
      conditions[x.id] = {
        [db.getOperator(db[model].rawAttributes[x.id].type.toString(), Op)]:
          db.getValue(db[model].rawAttributes[x.id].type.toString(), x.value),
      };
    });
  }

  // order
  if (req.query.sort !== undefined && req.query.sort.length > 0) {
    req.query.sort[0] = JSON.parse(req.query.sort[0]);
    orders = [[req.query.sort[0].id, req.query.sort[0].desc ? 'DESC' : 'ASC']];
  }

  try {
    let data = await db[model].findAll({
      where: conditions,
      order: orders,
    });

    const csvFields = Object.keys(db[model].rawAttributes);
    const csvParser = new CsvParser({ csvFields });
    const csvData = csvParser.parse(data.map((x) => x.dataValues));

    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', 'attachment; filename=export.csv');

    res.status(200).end(csvData);
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message: err.message || 'Some error occurred while retrieving data.',
    });
  }
};

exports.create = async (req, res) => {
  let model = req.query.model;

  try {
    let request = {};

    Object.keys(req.body).forEach((x) => {
      let value = req.body[x];
      if (value !== null) {
        if (value.toString() === '') {
          value = null;
        }
      }

      request[x] = value;
    });

    if (model === 'q_user') {
      const key = process.env.CIPHER_KEY;

      const original = request.password;
      const iv = process.env.CIPHER_IV;
      const cipher = new Rijndael(key, process.env.CIPHER_MODE);
      const ciphertext = Buffer.from(cipher.encrypt(original, 256, iv));
      request.password = ciphertext.toString('base64');
    }

    let data = await db[model].create(request);

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message: err.message || 'Some error ocurred while creating the ' + model,
    });
  }
};

exports.update = async (req, res) => {
  let model = req.query.model;

  try {
    let request = {};

    Object.keys(req.body).forEach((x) => {
      let value = req.body[x];
      if (value !== null) {
        if (value.toString() === '') {
          value = null;
        }
      }

      if (x !== 'rowid') {
        request[x] = value;
      }
    });

    if (model === 'q_user') {
      const key = process.env.CIPHER_KEY;

      const original = request.password;
      const iv = process.env.CIPHER_IV;
      const cipher = new Rijndael(key, process.env.CIPHER_MODE);
      const ciphertext = Buffer.from(cipher.encrypt(original, 256, iv));
      request.password = ciphertext.toString('base64');
    }

    let data = await db[model].update(request, { where: { rowid: req.body.rowid } });

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message: err.message || 'Some error ocurred while creating the ' + model,
    });
  }
};

exports.delete = async (req, res) => {
  let model = req.query.model;

  try {
    if (model === 'q_pools') {
      let groups = [];

      let queryGroups = 'SELECt * FROM q_groups WHERE poolId = '+ req.query.id;
      let resultGroups = await db.sequelize.query(queryGroups);

      if (resultGroups[0].length > 0) {
        const forLoop = async _ => {
          for (let index = 0; index < resultGroups[0].length; index++) {

            // search all the teams by group
            let queryTeamGroups = 'DELETE FROM q_group_team WHERE groupId = '+ resultGroups[0][index].rowid;
            let resultTeamGroups = await db.sequelize.query(queryTeamGroups);
          }
        };

        await forLoop();
      }

      queryGroups = 'DELETE FROM q_groups WHERE poolId = '+ req.query.id;
      resultGroups = await db.sequelize.query(queryGroups);
    }

    let data = await db[model].destroy({
      where: { rowid: { [Op.eq]: req.query.id } },
    });

    res.send({
      code: 'D200',
      result: data,
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
        err.message ||
        `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};
