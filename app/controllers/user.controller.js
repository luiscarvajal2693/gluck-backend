const db = require('../models');
const Op = db.Sequelize.Op;

exports.rechargue = async (req, res) => {

  let user = await db.q_user.findOne({
    where: { rowid: { [Op.eq]: req.body.userId } }
  });

  if (req.body.type === 'amount') {
    user.dataValues.amount += Number(req.body.amount)
  } else {
    user.dataValues.coins += Number(req.body.amount)
  }

  try {
    let data = await db.q_user.update(user.dataValues, { where: { rowid: req.body.userId } });

    const dataTransaction = await db.q_transaction.create({
      username: user.dataValues.username,
      amount: req.body.type === 'amount' ? Number(req.body.amount) : 0,
      coins:  req.body.type !== 'amount' ? Number(req.body.amount) : 0,
      description: 'El usuario ' + user.dataValues.username + ' Ha Recargado a su cuenta!'
    });

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
          err.message ||
          `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};