const db = require('../models');
const Op = db.Sequelize.Op;
const moment = require('moment');

function getNumber(n) {
  return n ? n : 0;
}

function isNullOrEmptyNumber(n) {
  return n === null || n === undefined;
}

function getResult(resultUser1, resultUser2, resultAdmin1, resultAdmin2, result, pool, penalty1 = 0, penalty2 = 0) {
  let scoreTotal = 0;

  puntaje_empate = Number(pool['puntaje_empate']);

  puntaje_ganar = Number(pool['puntaje_ganar']);

  puntaje_perder = Number(pool['puntaje_perder']);

  puntaje_resultado = Number(pool['puntaje_resultado']);

  //verificamos si es el resultado exacto

  if (resultUser1 == resultAdmin1 && resultUser2 == resultAdmin2)

    scoreTotal += puntaje_resultado;

  //verificamos si hay empate

  if (resultUser1 == resultUser2 && resultAdmin1 == resultAdmin2)

    scoreTotal += puntaje_empate;

  //verificamos si gano el equipo uno gano o perdio

  scoreTotal += (resultUser1 > resultUser2 && resultAdmin1 > resultAdmin2) ? puntaje_ganar : puntaje_perder;

  //verificamos si gano el equipo dos gano o perdio

  scoreTotal += (resultUser1 < resultUser2 && resultAdmin1 < resultAdmin2) ? puntaje_ganar : puntaje_perder;

  if (result == "C" || result == "S") {
    scoreTotal = 0;
  }

  return scoreTotal;
}

exports.getResultsByPool = async (req, res) => {

  let pool = await db.q_pools.findOne({
    where: { rowid: { [Op.eq]: req.query.poolId } }
  });

  let data = [];
  let query = 'SELECT DISTINCT u.rowid as userId, u.name,u.lastname,u.email,u.phone, u.img, u.username, (SELECT sum(qrp.hits) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools) ranking, (SELECT sum(qrp.team__result_1) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_1 = qrp.team__result_1_admin) goles1, (SELECT sum(qrp.team__result_2) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_2 = qrp.team__result_2_admin) goles2, u.date_Access, u.rowid as rowid_user, r.fk_q_pools as result FROM q_user u, q_result_pools r, q_user_pools up WHERE u.rowid = up.fk_q_user AND r.fk_q_pools = up.fk_q_pools AND r.fk_q_pools = '+req.query.poolId+'  ORDER BY r.rowid DESC';
  try {
    let result = await db.sequelize.query(query);
    // console.log(result);

    const forLoop = async _ => {
      for (let index = 0; index < result[0].length; index++) {
        if (result[0][index].ranking != null) {
          query = 'SELECT qrp.rowid, qrp.fk_q_user,qrp.fk_q_pools,qrp.fk_team_1,qrp.team__result_1,qrp.team__result_1_admin,qrp.fk_team_2,qrp.team__result_2,qrp.team__result_2_admin,qrp.status,qrp.comment,qrp.result_admin,qrp.date_Sport,qrp.hour, qrp.hits,qrp.close,qrp.date_Create, penalty1, penalty2 , (select name from q_sport where qrp.fk_q_user=rowid) as sport,(SELECT result FROM q_pools_details WHERE fk_pools = '+req.query.poolId+' AND fk_team_1 = qrp.fk_team_1 AND fk_team_2 = qrp.fk_team_2 LIMIT 1) as result FROM q_result_pools qrp WHERE qrp.fk_q_pools = '+req.query.poolId+' AND qrp.fk_q_user = '+ result[0][index].userId;
          let result2 = await db.sequelize.query(query);

          let restFinal = 0;
          let sumResFinal = 0;

          const forLoop2 = async _ => {
            for (let index2 = 0; index2 < result2[0].length; index2++) {
              restFinal = getResult(Number(result2[0][index2].team__result_1), Number(result2[0][index2].team__result_2), Number(result2[0][index2].team__result_1_admin), Number(result2[0][index2].team__result_2_admin) ,result2[0][index2].result, pool.dataValues, getNumber(result2[0][index2].penalty1), getNumber(result2[0][index2].penalty2));
              sumResFinal += restFinal;
            }
          }

          await forLoop2();

          data.push({
            userID: result[0][index].userId,
            name: result[0][index].name + ' ' + result[0][index].lastname,
            email: result[0][index].email,
            username: result[0][index].username,
            phone: result[0][index].phone,
            img: result[0][index].img,
            date_Access: result[0][index].date_Access,
            goles: Number(result[0][index].goles1) + Number(result[0][index].goles2),
            resultado: sumResFinal
          })
        } else {
          data.push({
            userID: result[0][index].userId,
            name: result[0][index].name + ' ' + result[0][index].lastname,
            email: result[0][index].email,
            username: result[0][index].username,
            phone: result[0][index].phone,
            img: result[0][index].img,
            date_Access: result[0][index].date_Access,
            goles: 0,
            resultado: 0
          })
        }
      }
    };

    await forLoop();

    data.sort((a,b) => (a.goles < b.goles) ? 1 : ((b.goles < a.goles) ? -1 : 0))
    data.sort((a,b) => (a.resultado < b.resultado) ? 1 : ((b.resultado < a.resultado) ? -1 : 0))

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
        err.message ||
        `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

exports.getResultsUserForClient = async (req, res) => {

  try {

    let query = 'SELECT * FROM q_pools p left join q_user_pools qup on p.rowid = qup.fk_q_pools WHERE p.status = 2 AND qup.fk_q_user = ' + req.query.userId;
    let resultPools = await db.sequelize.query(query);

    let data = [];

    const forLoop0 = async _ => {
      for (let index0 = 0; index0 < resultPools[0].length; index0++) {
        query = 'SELECT DISTINCT u.rowid as userId, u.name,u.lastname,u.email,u.phone,(SELECT sum(qrp.hits) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools) ranking, (SELECT sum(qrp.team__result_1) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_1 = qrp.team__result_1_admin) goles1, (SELECT sum(qrp.team__result_2) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_2 = qrp.team__result_2_admin) goles2, u.date_Access, u.rowid as rowid_user, r.fk_q_pools as result FROM q_user u, q_result_pools r WHERE u.rowid=r.fk_q_user AND r.fk_q_pools = ' +  resultPools[0][index0].fk_q_pools + ' AND r.fk_q_user = ' + req.query.userId + ' ORDER BY r.rowid DESC';
        let result = await db.sequelize.query(query);
        // console.log(result);

        const forLoop = async _ => {
          for (let index = 0; index < result[0].length; index++) {
            query = 'SELECT qrp.rowid, qrp.fk_q_user,qrp.fk_q_pools,qrp.fk_team_1,qrp.team__result_1,qrp.team__result_1_admin,qrp.fk_team_2,qrp.team__result_2,qrp.team__result_2_admin,qrp.status,qrp.comment,qrp.result_admin,qrp.date_Sport,qrp.hour, qrp.hits,qrp.close,qrp.date_Create , (select name from q_sport where qrp.fk_q_user=rowid) as sport,(SELECT result FROM q_pools_details WHERE fk_pools = ' + resultPools[0][index0].fk_q_pools + ' AND fk_team_1 = qrp.fk_team_1 AND fk_team_2 = qrp.fk_team_2) as result FROM q_result_pools qrp WHERE qrp.fk_q_pools = ' + resultPools[0][index0].fk_q_pools + ' AND qrp.fk_q_user = ' + result[0][index].userId;
            let result2 = await db.sequelize.query(query);

            let restFinal = 0;
            let sumResFinal = 0;

            const forLoop2 = async _ => {
              for (let index2 = 0; index2 < result2[0].length; index2++) {
                restFinal = getResult(Number(result2[0][index2].team__result_1), Number(result2[0][index2].team__result_2), Number(result2[0][index2].team__result_1_admin), Number(result2[0][index2].team__result_2_admin), result2[0][index2].result, resultPools[0][index0]);
                sumResFinal += restFinal;
              }
            }

            await forLoop2();

            data.push({
              id: resultPools[0][index0].fk_q_pools,
              name: resultPools[0][index0].name,
              goles: Number(result[0][index].goles1) + Number(result[0][index].goles2),
              resultado: sumResFinal
            })
          }
        };

        await forLoop();
      }
    }

    await forLoop0();

    res.send({
      data: data,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
          err.message ||
          `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

exports.getEventsForUser = async (req, res) => {

  try {

    let query = 'SELECT p.rowid,p.name,d.date_Sport,p.fk_sport,p.color,p.quantity,hour,label FROM q_pools p, q_pools_details d  WHERE p.rowid=d.fk_pools AND p.status=2 AND p.rowid IN (SELECT p.rowid FROM q_user_pools u, q_pools p WHERE p.rowid=u.fk_q_pools AND u.fk_q_user = '+req.query.userId+') ORDER BY d.date_Sport DESC';
    let resultEvents = await db.sequelize.query(query);

    res.send({
      data: resultEvents[0],
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
          err.message ||
          `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

function getResultsForTeam(teamId, matches) {
  let winMatches = 0;
  let loseMatches = 0;
  let drawMatches = 0;

  let goalsFor = 0;
  let goalsAgainst = 0;

  let points = 0;

  matches.forEach(match => {
    if (match.fk_team_1 === teamId) {
      if (match.result_team_1 > match.result_team_2) {
        winMatches++;
        points+=3
      } else if (match.result_team_1 < match.result_team_2) {
        loseMatches++;
      } else {
        drawMatches++;
        points += 1;
      }

      goalsFor += match.result_team_1;
      goalsAgainst += match.result_team_2;
    }

    if (match.fk_team_2 === teamId) {
      if (match.result_team_2 > match.result_team_1) {
        winMatches++;
        points+=3
      } else if (match.result_team_2 < match.result_team_1) {
        loseMatches++;
      } else {
        drawMatches++;
        points += 1;
      }

      goalsFor += match.result_team_2;
      goalsAgainst += match.result_team_1;
    }
  })
  return {
    winMatches,
    loseMatches,
    drawMatches,
    goalsFor,
    goalsAgainst,
    points
  };
}

async function setMatchData(matches) {
  const forLoop1 = async _ => {
    for (let index = 0; index < matches.length; index++) {
      let team1 = await db.q_team.findOne({
        where: {rowid: {[Op.eq]: matches[index].fk_team_1}}
      });
      let team2 = await db.q_team.findOne({
        where: {rowid: {[Op.eq]: matches[index].fk_team_2}}
      });

      matches[index].teamName1 = team1.dataValues.name;
      matches[index].teamImage1 = team1.dataValues.img;
      matches[index].teamName2 = team2.dataValues.name;
      matches[index].teamImage2 = team2.dataValues.img;
    }
  }

  await forLoop1();
}

async function getTeamById(teamId) {
  let team = await db.q_team.findOne({
    where: {rowid: {[Op.eq]: teamId}}
  });

  return team;
}

exports.getRankingForPool = async (req, res) => {

  try {
    let groups = [];

    let queryGroups = 'SELECt * FROM q_groups WHERE poolId = '+req.query.poolId;
    let resultGroups = await db.sequelize.query(queryGroups);

    if (resultGroups[0].length > 0) {
      const forLoop = async _ => {
        for (let index = 0; index < resultGroups[0].length; index++) {
          let queryGroupsMatches = 'SELECT * FROM q_pools_details WHERE fk_pools = '+ req.query.poolId +' AND (fk_team_1 in (SELECT teamId FROM q_group_team WHERE groupId = '+ resultGroups[0][index].rowid +') OR fk_team_2 in (SELECT teamId FROM q_group_team WHERE groupId = '+ resultGroups[0][index].rowid +')) AND bracket_type IS NULL ORDER BY date_Sport ASC';
          let resultGroupsMatches = await db.sequelize.query(queryGroupsMatches);
          let dates = [];

          await setMatchData(resultGroupsMatches[0]);

          resultGroupsMatches[0].forEach(match => {
            let index = dates.map(x => x.date).indexOf(match.date_Sport);

            if (index === -1) {
              dates.push({
                date: match.date_Sport,
                matches: [match]
              });
            } else {
              dates[index].matches.push(match);
            }
          })

          // search all the teams by group
          let queryTeamGroups = 'SELECT teamId FROM q_group_team WHERE groupId = '+ resultGroups[0][index].rowid;
          let resultTeamGroups = await db.sequelize.query(queryTeamGroups);

          let resultsForTeam = [];
          const forLoop1 = async _ => {
            for (let index = 0; index < resultTeamGroups[0].length; index++) {
              let teamId = resultTeamGroups[0][index].teamId;
              let resultForTeam = getResultsForTeam(teamId, resultGroupsMatches[0]);
              let team = await getTeamById(teamId);
              resultForTeam.teamId = teamId;
              resultForTeam.name = team.name;
              resultForTeam.img = team.img;

              resultsForTeam.push(resultForTeam)
            }
          }

          await forLoop1();

          resultsForTeam.sort((a,b) => (a.goalsFor < b.goalsFor) ? 1 : ((b.goalsFor < a.goalsFor) ? -1 : 0))
          resultsForTeam.sort((a,b) => (a.winMatches < b.winMatches) ? 1 : ((b.winMatches < a.winMatches) ? -1 : 0))
          resultsForTeam.sort((a,b) => (a.points < b.points) ? 1 : ((b.points < a.points) ? -1 : 0))

          groups.push({
            name: resultGroups[0][index].name,
            dates: dates,
            results: resultsForTeam,
          })
        }
      };

      await forLoop();

      let queryBrackets = 'SELECT * FROM q_pools_details WHERE bracket_type IS NOT NULL AND fk_pools =' + req.query.poolId;
      let resultBrackets = await db.sequelize.query(queryBrackets);

      let champion = null;

      await setMatchData(resultBrackets[0]);

      resultBrackets[0].forEach(match => {
        if (match.bracket_type === 'Final') {
          if (match.result_team_1 > match.result_team_2) {
            champion = {
              name: match.teamName1,
              img: match.teamImage1
            }
          } else {
            champion = {
              name: match.teamName2,
              img: match.teamImage2
            }
          }
        }
      })

      res.send({
        groups: groups,
        brackets: resultBrackets[0],
        champion: champion,
        code: 'D200',
      });
    } else {
      let queryGroupsMatches = 'SELECT * FROM q_pools_details WHERE fk_pools = '+ req.query.poolId;
      let resultGroupsMatches = await db.sequelize.query(queryGroupsMatches);
      let dates = [];

      await setMatchData(resultGroupsMatches[0]);

      resultGroupsMatches[0].forEach(match => {
        let index = dates.map(x => x.date).indexOf(match.date_Sport);

        if (index === -1) {
          dates.push({
            date: match.date_Sport,
            matches: [match]
          });
        } else {
          dates[index].matches.push(match);
        }
      })

      // search all the teams by group
      let queryTeamGroups = 'SELECT fk_team_1 as teamId FROM q_pools_details WHERE fk_pools = '+ req.query.poolId +' UNION SELECT fk_team_2 as teamId FROM q_pools_details WHERE fk_pools = '+ req.query.poolId;
      let resultTeamGroups = await db.sequelize.query(queryTeamGroups);

      let resultsForTeam = [];
      const forLoop1 = async _ => {
        for (let index = 0; index < resultTeamGroups[0].length; index++) {
          let teamId = resultTeamGroups[0][index].teamId;
          let resultForTeam = getResultsForTeam(teamId, resultGroupsMatches[0]);
          let team = await getTeamById(teamId);
          resultForTeam.teamId = teamId;
          resultForTeam.name = team.dataValues.name;
          resultForTeam.img = team.dataValues.img;

          resultsForTeam.push(resultForTeam)
        }
      }

      await forLoop1();

      resultsForTeam.sort((a,b) => (a.goalsFor < b.goalsFor) ? 1 : ((b.goalsFor < a.goalsFor) ? -1 : 0))
      resultsForTeam.sort((a,b) => (a.winMatches < b.winMatches) ? 1 : ((b.winMatches < a.winMatches) ? -1 : 0))
      resultsForTeam.sort((a,b) => (a.points < b.points) ? 1 : ((b.points < a.points) ? -1 : 0))

      groups.push({
        name: '',
        dates: dates,
        results: resultsForTeam,
      })

      //no groups
      res.send({
        groups: groups,
        brackets: [],
        champion: null,
        code: 'D200',
      });
    }

  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
          err.message ||
          `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

exports.getResultsByPoolAndUser = async (req, res) => {

  let pool = await db.q_pools.findOne({
    where: { rowid: { [Op.eq]: req.query.poolId } }
  });
  let user = await db.q_user.findOne({
    where: { rowid: { [Op.eq]: req.query.userId } }
  });

  let data = [];
  let query = 'SELECT qrp.rowid, qrp.fk_team_1, qrp.fk_team_2, qrp.fk_q_user,qrp.fk_q_pools,qrp.fk_team_1,qrp.team__result_1,qrp.team__result_1_admin,qrp.fk_team_2,qrp.team__result_2,qrp.team__result_2_admin,qrp.status,qrp.comment,qrp.result_admin,qrp.date_Sport,qrp.hour, qrp.hits,qrp.close,qrp.date_Create , (select name from q_sport where qrp.fk_q_user=rowid) as sport, (select img from q_team where qrp.fk_team_1 = rowid) as image1,(select img from q_team where qrp.fk_team_2 = rowid) as image2,(SELECT result FROM q_pools_details WHERE fk_pools = '+req.query.poolId+' AND fk_team_1 = qrp.fk_team_1 AND fk_team_2 = qrp.fk_team_2 LIMIT 1) as result, (SELECT concat( qpd.label ,\' \', qpd.date_Sport,\' \',qpd.hour) as sportLabel FROM q_pools_details qpd WHERE fk_pools = '+req.query.poolId+' AND fk_team_1 = qrp.fk_team_1 AND fk_team_2 = qrp.fk_team_2  LIMIT 1) as sportLabel, (SELECT name FROM q_team WHERE q_team.rowid = qrp.fk_team_1  LIMIT 1) as teamName1, (SELECT name FROM q_team WHERE q_team.rowid = qrp.fk_team_2  LIMIT 1) as teamName2 FROM q_result_pools qrp WHERE qrp.fk_q_pools = '+req.query.poolId+' AND qrp.fk_q_user = '+ req.query.userId + ' ORDER BY qrp.rowid asc';
  try {
    let result = await db.sequelize.query(query);
    // console.log(result);

    let restFinal = 0;
    let sumResFinal = 0;

    const forLoop = async _ => {
      for (let index = 0; index < result[0].length; index++) {
        restFinal = getResult(Number(result[0][index].team__result_1), Number(result[0][index].team__result_2), Number(result[0][index].team__result_1_admin), Number(result[0][index].team__result_2_admin) ,result[0][index].result, pool.dataValues );
        sumResFinal += restFinal;

        data.push({
          teamId1: result[0][index].fk_team_1,
          teamId2: result[0][index].fk_team_2,
          sportLabel: result[0][index].sportLabel,
          image1: result[0][index].image1,
          image2: result[0][index].image2,
          teamName1: result[0][index].teamName1,
          teamName2: result[0][index].teamName2,
          teamResult1: result[0][index].team__result_1,
          teamResult2: result[0][index].team__result_2,
          teamResult1Admin: result[0][index].team__result_1_admin,
          teamResult2Admin: result[0][index].team__result_2_admin,
          date: result[0][index].date_Sport,
          hour: result[0][index].hour,
          status: result[0][index].result,
          resultado: restFinal
        })
      }
    };

    await forLoop();

    data.sort((a,b) => (a.resultado < b.resultado) ? 1 : ((b.resultado < a.resultado) ? -1 : 0))

    let queryGoles = 'SELECT DISTINCT\n' +
        '                (SELECT sum(qrp.team__result_1) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_1 = qrp.team__result_1_admin) goles1,\n' +
        '                (SELECT sum(qrp.team__result_2) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_2 = qrp.team__result_2_admin) goles2\n' +
        'FROM q_user u, q_result_pools r\n' +
        'WHERE u.rowid=r.fk_q_user AND r.fk_q_pools = '+req.query.poolId+' AND u.rowid = '+req.query.userId+'  ORDER BY r.rowid DESC'
    let resultGoles = await db.sequelize.query(queryGoles);


    res.send({
      data: data,
      resultado: sumResFinal,
      goles: Number(resultGoles[0][0].goles1) + Number(resultGoles[0][0].goles2),
      username: user.dataValues.username,
      poolName: pool.dataValues.name,
      dateCreate: pool.dataValues.date_Create,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
        err.message ||
        `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

exports.getResultsByPoolAndUserWithoutSort = async (req, res) => {

  let pool = await db.q_pools.findOne({
    where: { rowid: { [Op.eq]: req.query.poolId } }
  });
  let user = await db.q_user.findOne({
    where: { rowid: { [Op.eq]: req.query.userId } }
  });

  let data = [];
  let query = 'SELECT qrp.rowid, qrp.fk_team_1, qrp.fk_team_2, qrp.fk_q_user,qrp.fk_q_pools,qrp.fk_team_1,qrp.team__result_1,qrp.team__result_1_admin,qrp.fk_team_2,qrp.team__result_2,qrp.team__result_2_admin,qrp.status,qrp.comment,qrp.result_admin,qrp.date_Sport,qrp.hour, qrp.hits,qrp.close,qrp.date_Create, penalty1, penalty2 , (select name from q_sport where qrp.fk_q_user=rowid) as sport, (select img from q_team where qrp.fk_team_1 = rowid) as image1,(select img from q_team where qrp.fk_team_2 = rowid) as image2,(SELECT result FROM q_pools_details WHERE fk_pools = '+req.query.poolId+' AND fk_team_1 = qrp.fk_team_1 AND fk_team_2 = qrp.fk_team_2 LIMIT 1) as result, (SELECT concat( qpd.label ,\' \', qpd.date_Sport,\' \',qpd.hour) as sportLabel FROM q_pools_details qpd WHERE fk_pools = '+req.query.poolId+' AND fk_team_1 = qrp.fk_team_1 AND fk_team_2 = qrp.fk_team_2  LIMIT 1) as sportLabel, (SELECT name FROM q_team WHERE q_team.rowid = qrp.fk_team_1  LIMIT 1) as teamName1, (SELECT name FROM q_team WHERE q_team.rowid = qrp.fk_team_2  LIMIT 1) as teamName2 FROM q_result_pools qrp WHERE qrp.fk_q_pools = '+req.query.poolId+' AND qrp.fk_q_user = '+ req.query.userId + ' ORDER BY qrp.rowid asc';
  try {
    let result = await db.sequelize.query(query);
    // console.log(result);

    let restFinal = 0;
    let sumResFinal = 0;

    const forLoop = async _ => {
      for (let index = 0; index < result[0].length; index++) {
        restFinal = getResult(Number(result[0][index].team__result_1), Number(result[0][index].team__result_2), Number(result[0][index].team__result_1_admin), Number(result[0][index].team__result_2_admin) ,result[0][index].result, pool.dataValues );
        sumResFinal += restFinal;

        data.push({
          teamId1: result[0][index].fk_team_1,
          teamId2: result[0][index].fk_team_2,
          sportLabel: result[0][index].sportLabel,
          image1: result[0][index].image1,
          image2: result[0][index].image2,
          teamName1: result[0][index].teamName1,
          teamName2: result[0][index].teamName2,
          teamResult1: result[0][index].team__result_1,
          teamResult2: result[0][index].team__result_2,
          teamResult1Admin: result[0][index].team__result_1_admin,
          teamResult2Admin: result[0][index].team__result_2_admin,
          penalty1: getNumber(result[0][index].penalty1),
          penalty2: getNumber(result[0][index].penalty2),
          date: result[0][index].date_Sport,
          hour: result[0][index].hour,
          status: result[0][index].result,
          resultado: restFinal
        })
      }
    };

    await forLoop();

    // data.sort((a,b) => (a.resultado < b.resultado) ? 1 : ((b.resultado < a.resultado) ? -1 : 0))

    let queryGoles = 'SELECT DISTINCT\n' +
        '                (SELECT sum(qrp.team__result_1) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_1 = qrp.team__result_1_admin) goles1,\n' +
        '                (SELECT sum(qrp.team__result_2) FROM q_result_pools qrp where qrp.fk_q_user=u.rowid and qrp.fk_q_pools=r.fk_q_pools and qrp.team__result_2 = qrp.team__result_2_admin) goles2\n' +
        'FROM q_user u, q_result_pools r\n' +
        'WHERE u.rowid=r.fk_q_user AND r.fk_q_pools = '+req.query.poolId+' AND u.rowid = '+req.query.userId+'  ORDER BY r.rowid DESC'
    let resultGoles = await db.sequelize.query(queryGoles);


    res.send({
      data: data,
      resultado: sumResFinal,
      goles: Number(resultGoles[0][0].goles1) + Number(resultGoles[0][0].goles2),
      username: user.dataValues.username,
      poolName: pool.dataValues.name,
      dateCreate: pool.dataValues.date_Create,
      code: 'D200',
    });
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
        err.message ||
        `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};

exports.updateResultByUserAndPool = async (req, res) => {
  let data = [];
  try {

    let pool = await db.q_pools.findOne({
      where: {rowid: {[Op.eq]: req.body.id}}
    });

    let dateSaved = moment(pool.dataValues.dateFinish+'T'+pool.dataValues.timeFinish);

    if (dateSaved.isBefore(moment().utcOffset('-0400'))) {
      res.send({
        message: 'La quiniela ya no esta disponible para actualizar resultados!',
        code: 'D400',
        dateSaved: dateSaved.format('YYYY-MM-DDTHH:mm:ss:SSS'),
        date: moment().utcOffset('-0400').format('YYYY-MM-DDTHH:mm:ss:SSS')
      });
    } else {
      const forLoop = async _ => {
        for (let index = 0; index < req.body.matchesInfo.length; index++) {
          let queryDeleteResults = `DELETE FROM q_result_pools WHERE fk_q_user = ${req.query.userId} AND  fk_q_pools = ${req.body.id} AND fk_team_1 = ${req.body.matchesInfo[index].team1} AND fk_team_2 = ${req.body.matchesInfo[index].team2}`;
          let resultDeleteResults = await db.sequelize.query(queryDeleteResults);
          data.push(resultDeleteResults)

        }

        for (let index = 0; index < req.body.matchesInfo.length; index++) {
          let queryInsertResults
          if (!isNullOrEmptyNumber(req.body.matchesInfo[index].penalty1) && !isNullOrEmptyNumber(req.body.matchesInfo[index].penalty2)) {
            queryInsertResults = `INSERT INTO q_result_pools (fk_q_user, fk_q_pools, fk_team_1, team__result_1, fk_team_2, team__result_2, status,date_Sport,hour, penalty1, penalty2) VALUES (${req.query.userId},${req.body.id},${req.body.matchesInfo[index].team1},${req.body.matchesInfo[index].resultTeam1},${req.body.matchesInfo[index].team2},${req.body.matchesInfo[index].resultTeam2}, ${req.body.matchesInfo[index].status},'${req.body.matchesInfo[index].date}', '${req.body.matchesInfo[index].time}', ${req.body.matchesInfo[index].penalty1}, ${req.body.matchesInfo[index].penalty2}) `;
          } else {
            queryInsertResults = `INSERT INTO q_result_pools (fk_q_user, fk_q_pools, fk_team_1, team__result_1, fk_team_2, team__result_2, status,date_Sport,hour) VALUES (${req.query.userId},${req.body.id},${req.body.matchesInfo[index].team1},${req.body.matchesInfo[index].resultTeam1},${req.body.matchesInfo[index].team2},${req.body.matchesInfo[index].resultTeam2}, ${req.body.matchesInfo[index].status},'${req.body.matchesInfo[index].date}', '${req.body.matchesInfo[index].time}') `;
          }
          let resultInsertResults = await db.sequelize.query(queryInsertResults);
          data.push(resultInsertResults);
        }
      };

      await forLoop();


      res.send({
        data: data,
        code: 'D200',
        dateSaved: dateSaved.format('YYYY-MM-DDTHH:mm:ss:SSS'),
        date: moment().utcOffset('-0400').format('YYYY-MM-DDTHH:mm:ss:SSS')
      });
    }
  } catch (err) {
    res.status(200).send({
      code: 'D500',
      message:
        err.message ||
        `Some error occurred while retrieving id = ${req.query.id} .`,
    });
  }
};
