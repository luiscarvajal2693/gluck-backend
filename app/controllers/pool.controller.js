

const db = require('../models');
const Op = db.Sequelize.Op;
const axios = require("axios");
const moment = require('moment');

function getInteger(value) {
    return (value === null || value === '' || value  === undefined) ? 0 : value
}

async function registerPool(userId, poolId) {
    let resultResponse = await axios({
        method: "post",
        url: `${process.env.BACKEND_URL}/api/pool/registerUserPool`,
        responseType: "application/json",
        data: {
            poolId: poolId,
            userId: userId
        },
    })

    console.log(JSON.stringify(resultResponse.data));
}

async function unRegisterUserPool(userId, poolId) {
    let resultResponse = await axios({
        method: "post",
        url: `${process.env.BACKEND_URL}/api/pool/unRegisterUserPool`,
        responseType: "application/json",
        data: {
            poolId: poolId,
            userId: userId
        },
    })

    console.log(JSON.stringify(resultResponse.data));
}

exports.create = async (req, res) => {

    try {
        //save Pool
        let queryPool = `INSERT INTO q_pools (name, password, fk_sport, quantity, limit_user, color, status, penalty,
                                              puntaje_empate,
                                              puntaje_ganar, puntaje_perder, puntaje_resultado, pool_type, league,tournament_type,
                                              groups, hot, groupsTeam, rules, amountInput, coinsInput,
                                              dateFinish, timeFinish, awardType, awardValue)
                         VALUES ('${req.body.name}', '${req.body.password}', ${req.body.sport}, ${req.body.matches},
                                 ${req.body.usersLimit},
                                 '${req.body.color}', ${req.body.status}, ${req.body.penalty}, '${req.body.draw}',
                                 '${req.body.winner}',
                                 '${req.body.loser}', '${req.body.result}', '${req.body.type}', '${req.body.league}', '${req.body.tournamentType}',
                                 ${getInteger(req.body.groups)},
                                 ${req.body.hot},
                                 ${getInteger(req.body.teamsPerGroup)}, '${req.body.rules}',
                                 ${getInteger(req.body.amountInput)},
                                 ${getInteger(req.body.coinsInput)}, '${req.body.dateFinish}', '${req.body.timeFinish}',
                                 '${req.body.awardType}', ${getInteger(req.body.awardValue)})`;

        let resultPool = await db.sequelize.query(queryPool);

        const forLoop = async _ => {
            for (let index = 0; index < req.body.matchesInfo.length; index++) {
                let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status, result,
                                                               number_pools, label, penalty)
                                  VALUES (${resultPool[0]}, ${req.body.matchesInfo[index].team1},
                                          ${getInteger(req.body.matchesInfo[index].result1)},
                                          ${getInteger(req.body.matchesInfo[index].penalty1)},
                                          ${req.body.matchesInfo[index].team2},
                                          ${getInteger(req.body.matchesInfo[index].result2)},
                                          ${getInteger(req.body.matchesInfo[index].penalty2)},
                                          '${req.body.matchesInfo[index].date}', '${req.body.matchesInfo[index].time}',
                                          ${req.body.matchesInfo[index].status}, '${req.body.matchesInfo[index].result}
                                          ', ${(index + 1)},
                                          '${req.body.matchesInfo[index].title}', ${req.body.penalty})`;
                let resultMatch = await db.sequelize.query(queryMatch);
            }
        };

        await forLoop();


        const forLoop2 = async _ => {
            for (let index = 0; index < req.body.usersForPool.length; index++) {
                // let queryUser = `INSERT INTO q_user_pools (fk_q_user, fk_q_pools)
                //                  VALUES (${req.body.usersForPool[index]}, ${resultPool[0]})`;
                // let resultUser = await db.sequelize.query(queryUser);

                await registerPool(req.body.usersForPool[index], resultPool[0]);
            }
        };

        await forLoop2();

        //groups

        if (req.body.league === 'Copa America' || req.body.league === 'generic') {
            const forLoop3 = async _ => {
                for (let index = 0; index < req.body.groupsInfo.length; index++) {
                    let queryGroup = `INSERT INTO q_groups (name, poolId)
                                  VALUES ('${req.body.groupsInfo[index].name}', ${resultPool[0]})`;
                    let resultGroup = await db.sequelize.query(queryGroup);

                    const forLoop4 = async _ => {
                        for (let index2 = 0; index2 < req.body.groupsInfo[index].teams.length; index2++) {
                            let queryGroupTeam = `INSERT INTO q_group_team (groupId, teamId)
                                              VALUES (${resultGroup[0]}, ${req.body.groupsInfo[index].teams[index2]})`;
                            let resultGroupTeam = await db.sequelize.query(queryGroupTeam);
                        }
                    };

                    await forLoop4();
                }
            };

            await forLoop3();
        }

        res.send({
            data: resultPool,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.registerUserPool = async (req, res) => {

    try {

        // transaction
        let pool = await db.q_pools.findOne({
            where: {rowid: {[Op.eq]: req.body.poolId}}
        });

        let user = await db.q_user.findOne({
            where: {rowid: {[Op.eq]: req.body.userId}}
        });

        if (user.dataValues.coins >= pool.dataValues.coinsInput) {
            if (user.dataValues.amount >= pool.dataValues.amountInput) {
                user.dataValues.coins -= pool.dataValues.coinsInput;
                user.dataValues.amount -= pool.dataValues.amountInput;

                let dataUserUpdate = await db.q_user.update(user.dataValues, {where: {rowid: req.body.userId}});

                // if (pool.dataValues.awardType === 'total') {
                //     pool.dataValues.awardValue += pool.dataValues.amountInput
                //     let dataPoolUpdate = await db.q_pools.update(pool.dataValues, {where: {rowid: req.body.poolId}});
                // }

                const dataTransaction = await db.q_transaction.create({
                    username: user.dataValues.username,
                    amount: pool.dataValues.amountInput,
                    coins: pool.dataValues.coinsInput,
                    description: 'El usuario ' + user.dataValues.username + ' ha ingresado a la quiniela ' + pool.dataValues.name
                });

                let queryUser = `INSERT INTO q_user_pools (fk_q_user, fk_q_pools)
                                 VALUES (${req.body.userId}, ${req.body.poolId})`;
                let resultUser = await db.sequelize.query(queryUser);

                res.send({
                    data: resultUser,
                    code: 'D200',
                });

            } else {
                res.status(200).send({
                    code: 'D402',
                    message: 'El cliente no posee la cantidad necesaria de monto para ingresar!',
                });
            }
        } else {
            res.status(200).send({
                code: 'D401',
                message: 'El cliente no posee la cantidad necesaria de glucks para ingresar!',
            });
        }
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};


exports.unRegisterUserPool = async (req, res) => {

    try {

        // transaction
        let pool = await db.q_pools.findOne({
            where: {rowid: {[Op.eq]: req.body.poolId}}
        });

        let user = await db.q_user.findOne({
            where: {rowid: {[Op.eq]: req.body.userId}}
        });

        if (pool.dataValues.awardType === 'fijo') {
            //todo: revisar
            // pool.dataValues.awardValue -= pool.dataValues.amountInput
            // let dataPoolUpdate = await db.q_pools.update(pool.dataValues, {where: {rowid: req.body.poolId}});

            user.dataValues.coins += pool.dataValues.coinsInput;
            user.dataValues.amount += pool.dataValues.amountInput;

            let dataUserUpdate = await db.q_user.update(user.dataValues, {where: {rowid: req.body.userId}});

            let dataPoolUpdate = await db.q_pools.update(pool.dataValues, {where: {rowid: req.body.poolId}});

            const dataTransaction = await db.q_transaction.create({
                username: user.dataValues.username,
                amount: pool.dataValues.amountInput,
                coins: pool.dataValues.coinsInput,
                description: 'El usuario ' + user.dataValues.username + ' ha salido de la quiniela ' + pool.dataValues.name
            });

            let queryUserDeletePool = `DELETE FROM q_user_pools WHERE fk_q_pools = ${req.body.poolId} and fk_q_user = ${req.body.userId}`;
            let resultUser = await db.sequelize.query(queryUserDeletePool);

            res.send({
                data: resultUser,
                code: 'D200',
            });
        } else {
            res.send({
                data: {},
                code: 'D200',
            });
        }
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

function validateField(field) {
    return !(field === null || field === undefined);

}

function getResultTeamByBracket(bracket) {
    if (bracket.result_team_1 > bracket.result_team_2) {
        return {
            teamId: bracket.fk_team_1,
            name: bracket.teamName1
        };
    } else if(bracket.result_team_2 > bracket.result_team_1) {
        return {
            teamId: bracket.fk_team_2,
            name: bracket.teamName2
        };
    } else {
        if (bracket.result_team_1_penales_ > bracket.result_team_2_penales_) {
            return {
                teamId: bracket.fk_team_1,
                name: bracket.teamName1
            };
        } else {
            return {
                teamId: bracket.fk_team_2,
                name: bracket.teamName2
            };
        }
    }
}

function getResultTeamByBracketReverse(bracket) {
    if (bracket.result_team_1 > bracket.result_team_2) {
        return {
            teamId: bracket.fk_team_2,
            name: bracket.teamName2
        };
    } else if(bracket.result_team_2 > bracket.result_team_1) {
        return {
            teamId: bracket.fk_team_1,
            name: bracket.teamName1
        };
    } else {
        if (bracket.result_team_1_penales_ > bracket.result_team_2_penales_) {
            return {
                teamId: bracket.fk_team_2,
                name: bracket.teamName2
            };
        } else {
            return {
                teamId: bracket.fk_team_1,
                name: bracket.teamName1
            };
        }
    }
}

exports.update = async (req, res) => {

    try {
        //update Pool
        let queryPool = `UPDATE q_pools
                         SET name              = '${req.body.name}',
                             password          = '${req.body.password}',
                             fk_sport          = ${req.body.sport},
                             quantity          = ${req.body.matches},
                             limit_user        = ${req.body.usersLimit},
                             color             = '${req.body.color}',
                             status            = ${req.body.status},
                             penalty           = ${req.body.penalty},
                             puntaje_empate    = '${req.body.draw}',
                             puntaje_ganar     = '${req.body.winner}',
                             puntaje_perder    = '${req.body.loser}',
                             puntaje_resultado = '${req.body.result}',
                             pool_type         = '${req.body.type}',
                             league            = '${req.body.league}',
                             tournament_type            = '${req.body.tournamentType}',
                             groups            = ${req.body.groups},
                             hot            = ${req.body.hot},
                             groupsTeam        = ${req.body.teamsPerGroup},
                             rules             = '${req.body.rules}',
                             amountInput       = ${getInteger(req.body.amountInput)},
                             coinsInput        = ${getInteger(req.body.coinsInput)},
                             dateFinish        = '${req.body.dateFinish}',
                             timeFinish        = '${req.body.timeFinish}',
                             awardType         = '${req.body.awardType}',
                             awardValue        = ${getInteger(req.body.awardValue)}
                         WHERE rowid = ${req.body.id}`;
        let resultPool = await db.sequelize.query(queryPool);

        //groups

        // if (req.body.league === 'Copa America' || req.body.league === 'generic') {
        //     const forLoop3 = async _ => {
        //         for (let index = 0; index < req.body.groupsInfo.length; index++) {
        //             let queryGroup = `UPDATE q_groups
        //                                           SET name = '${req.body.groupsInfo[index].name}'
        //                                           WHERE poolId = ${resultPool[0]}`;
        //             let resultGroup = await db.sequelize.query(queryGroup);
        //         }
        //     };
        //
        //     await forLoop3();
        // }

        //update matches

        const forLoop = async _ => {
            for (let index = 0; index < req.body.matchesInfo.length; index++) {
                if (validateField(req.body.matchesInfo[index].id)) {
                    let queryMatch = `UPDATE q_pools_details
                                      SET fk_team_1              = ${req.body.matchesInfo[index].team1},
                                          result_team_1          = ${req.body.matchesInfo[index].result1},
                                          result_team_1_penales_ = ${req.body.matchesInfo[index].penalty1},
                                          fk_team_2              = ${req.body.matchesInfo[index].team2},
                                          result_team_2          = ${req.body.matchesInfo[index].result2},
                                          result_team_2_penales_ = ${req.body.matchesInfo[index].penalty2},
                                          date_Sport             = '${req.body.matchesInfo[index].date}',
                                          hour                   = '${req.body.matchesInfo[index].time}',
                                          status                 = ${req.body.matchesInfo[index].status},
                                          result                 = '${req.body.matchesInfo[index].result}',
                                          label                  = '${req.body.matchesInfo[index].title}',
                                          penalty                = ${req.body.penalty}
                                              ${validateField(req.body.matchesInfo[index].bracketType) ? (', bracket_type = \'' + req.body.matchesInfo[index].bracketType) + '\'' : ''}
                                      WHERE rowid = ${req.body.matchesInfo[index].id}`;
                    let resultMatch = await db.sequelize.query(queryMatch);

                    let querySearchResultsPoolsByMatch = `SELECT *
                                                          FROM q_result_pools
                                                          WHERE fk_q_pools = ${req.body.id}
                                                            AND fk_team_1 = ${req.body.matchesInfo[index].team1}
                                                            AND fk_team_2 = ${req.body.matchesInfo[index].team2}`;
                    let resultSearchResultsPoolsByMatch = await db.sequelize.query(querySearchResultsPoolsByMatch);

                    let querySearchResultsPools = `SELECT *
                                                   FROM q_result_pools
                                                   WHERE fk_q_pools = ${req.body.id}
                                                     AND fk_team_1 = ${req.body.matchesInfo[index].team1}
                                                     AND fk_team_2 = ${req.body.matchesInfo[index].team2}`;
                    let resultSearchResultsPools = await db.sequelize.query(querySearchResultsPools);

                    if (req.body.matchesInfo[index].result === 'E' || req.body.matchesInfo[index].result === 'T' || req.body.matchesInfo[index].result === 'F') {
                        let hits = 0;

                        const forLoop4 = async _ => {
                            for (let index2 = 0; index2 < resultSearchResultsPools[0].length; index2++) {
                                if (resultSearchResultsPools[0][index2].team__result_1 === req.body.matchesInfo[index].result1 &&
                                    resultSearchResultsPools[0][index2].team__result_2 === req.body.matchesInfo[index].result2 &&
                                    resultSearchResultsPools[0][index2].result_admin === req.body.matchesInfo[index].result
                                ) {
                                    hits++;
                                } else {
                                    hits = 0;
                                }
                            }
                        };

                        await forLoop4();

                        let queryUpdateResultPools = `UPDATE q_result_pools
                                                      SET team__result_1_admin = ${req.body.matchesInfo[index].result1},
                                                          team__result_2_admin = ${req.body.matchesInfo[index].result2},
                                                          result_admin         = '${req.body.matchesInfo[index].result}',
                                                          hits                 = ${hits},
                                                          close                = ${req.body.status}
                                                      WHERE fk_q_pools = ${req.body.id}
                                                        AND fk_team_1 = ${req.body.matchesInfo[index].team1}
                                                        AND fk_team_2 = ${req.body.matchesInfo[index].team2}`;
                        let resultUpdateResultPools = await db.sequelize.query(queryUpdateResultPools);

                        const forLoop5 = async _ => {
                            for (let index3 = 0; index3 < resultSearchResultsPoolsByMatch[0].length; index3++) {
                                let queryUpdateUserRanking = `UPDATE q_user
                                                              SET ranking = ranking + ${hits}
                                                              WHERE rowid = ${resultSearchResultsPoolsByMatch[0][index3].fk_q_user}`;
                                let resultUpdateUserRanking = await db.sequelize.query(queryUpdateUserRanking);
                            }
                        };

                        await forLoop5();
                    }

                } else {
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                                   result_team_1_penales_, fk_team_2, result_team_2,
                                                                   result_team_2_penales_, date_Sport, hour, status,
                                                                   result, number_pools, label, penalty, bracket_type)
                                      VALUES (${req.body.id}, ${req.body.matchesInfo[index].team1},
                                              ${getInteger(req.body.matchesInfo[index].result1)},
                                              ${getInteger(req.body.matchesInfo[index].penalty1)},
                                              ${req.body.matchesInfo[index].team2},
                                              ${getInteger(req.body.matchesInfo[index].result2)},
                                              ${getInteger(req.body.matchesInfo[index].penalty2)},
                                              '${req.body.matchesInfo[index].date}',
                                              '${req.body.matchesInfo[index].time}',
                                              ${req.body.matchesInfo[index].status},
                                              '${req.body.matchesInfo[index].result}', ${(index + 1)},
                                              '${req.body.matchesInfo[index].title}', ${req.body.penalty},
                                              '${req.body.matchesInfo[index].bracketType}')`;
                    let resultMatch = await db.sequelize.query(queryMatch);
                }

            }
        };

        await forLoop();

        let queryUserSearchPool = `SELECT *
                                   FROM q_user_pools
                                   WHERE fk_q_pools = ${req.body.id}`;
        let resultUserSearchPool = await db.sequelize.query(queryUserSearchPool);
        let usersInPool = resultUserSearchPool[0].map(x => x.fk_q_user);

        // let queryUserDeletePool = `DELETE FROM q_user_pools WHERE fk_q_pools = ${req.body.id}`;
        // let resultUserDeletePool = await db.sequelize.query(queryUserDeletePool);

        const forLoop3 = async _ => {
            for (let index = 0; index < req.body.usersForPool.length; index++) {
                if (!usersInPool.includes(req.body.usersForPool[index])) {
                    // let queryUser = `INSERT INTO q_user_pools (fk_q_user, fk_q_pools)
                    //                  VALUES (${req.body.usersForPool[index]}, ${req.body.id})`;
                    // let resultUser = await db.sequelize.query(queryUser);

                    await registerPool(req.body.usersForPool[index], req.body.id);
                }
            }

            //todo: tengo que buscar los usuarios que estan en la quiniela guardados pero no estan en la solicitud
            for (let index = 0; index < usersInPool; index++) {
                if (!req.body.usersForPool.includes(usersInPool[index])) {
                    await unRegisterUserPool(usersInPool[index], req.body.id);
                }
            }

        };

        await forLoop3();

        //brackets
        if (req.body.league === 'Copa America') {
            //verificar si las eliminatorias se cerraron
            let queryCounts = `SELECT
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type is null) As countEliminatorias,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Cuartos de Final') As countCuartos,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid AND bracket_type = 'Cuartos de Final') As countCuartosMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Semifinal') As countSemiFinal,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and bracket_type = 'Semifinal') As countSemiFinalMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Final') As countFinal,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and bracket_type = 'Final') As countFinalMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Mejor Tercero') As countMejorTercero,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and  bracket_type = 'Mejor Tercero') As countMejorTerceroMatches
                              FROM q_pools p
                              WHERE p.rowid = ${req.body.id}`;
            let resultCounts = await db.sequelize.query(queryCounts);

            let resultResponse = null;
            if (resultCounts[0][0].countEliminatorias === 0) {
                // evaluar puntaje de cada equipo por grupo
                resultResponse = await axios({
                    method: "get",
                    url: `${process.env.BACKEND_URL}/api/result/getRankingForPool?poolId=${req.body.id}`,
                    responseType: "appilcation/json"
                })

                console.log(JSON.stringify(resultResponse.data));
            }

            // creacion de 4tos
            if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countCuartosMatches === 0) {
                // crear los partidos A1 con B4, A2, con B3, B1 con A4, B2 con A3
                let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                                   result_team_1_penales_, fk_team_2, result_team_2,
                                                                   result_team_2_penales_, date_Sport, hour, status,
                                                                   result, number_pools, label, penalty, bracket_type)
                                      VALUES (${req.body.id}, 
                                              ${resultResponse.data.groups[0].results[0].teamId},
                                              0,
                                              0, 
                                              ${resultResponse.data.groups[1].results[3].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              1,
                                              '${resultResponse.data.groups[0].results[0].name + ' vs ' + resultResponse.data.groups[1].results[3].name}', 
                                              ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[0].results[1].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[1].results[2].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              2,
                                              '${resultResponse.data.groups[0].results[1].name + ' vs ' + resultResponse.data.groups[1].results[2].name}', 
                                              ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[1].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[0].results[3].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              3,
                                              '${resultResponse.data.groups[1].results[0].name + ' vs ' + resultResponse.data.groups[0].results[3].name}', 
                                              ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[1].results[1].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[0].results[2].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              4,
                                              '${resultResponse.data.groups[1].results[1].name + ' vs ' + resultResponse.data.groups[0].results[2].name}', 
                                              ${req.body.penalty},
                                              'Cuartos de Final')`;
                let resultMatch = await db.sequelize.query(queryMatch);

            }

            if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countCuartos === 0 && resultCounts[0][0].countSemiFinalMatches === 0) {
                let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status,
                                                               result, number_pools, label, penalty, bracket_type)
                                  VALUES (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[0]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[1]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          1,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[0]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[1]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Semifinal')
                                          ,
                                         (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[2]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[3]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          2,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[2]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[3]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Semifinal')`;
                let resultMatch = await db.sequelize.query(queryMatch);
            }

            if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countCuartos === 0 && resultCounts[0][0].countSemiFinal === 0 && resultCounts[0][0].countFinalMatches === 0) {
                let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status,
                                                               result, number_pools, label, penalty, bracket_type)
                                  VALUES (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[4]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[5]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          1,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[4]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[5]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Final')
                                          ,
                                         (${req.body.id},
                                          ${getResultTeamByBracketReverse(resultResponse.data.brackets[4]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracketReverse(resultResponse.data.brackets[5]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          2,
                                          '${getResultTeamByBracketReverse(resultResponse.data.brackets[2]).name + ' vs ' + getResultTeamByBracketReverse(resultResponse.data.brackets[3]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Mejor Tercero')`;
                let resultMatch = await db.sequelize.query(queryMatch);
            }




        }
        if (req.body.league === 'generic') {
            if (req.body.tournamentType === '8vos') {
                //verificar si las eliminatorias se cerraron
                let queryCounts = `SELECT
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type is null) As countEliminatorias,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Octavos de Final') As countOctavos,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid AND bracket_type = 'Octavos de Final') As countOctavosMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Cuartos de Final') As countCuartos,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid AND bracket_type = 'Cuartos de Final') As countCuartosMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Semifinal') As countSemiFinal,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and bracket_type = 'Semifinal') As countSemiFinalMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Final') As countFinal,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and bracket_type = 'Final') As countFinalMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Mejor Tercero') As countMejorTercero,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and  bracket_type = 'Mejor Tercero') As countMejorTerceroMatches
                              FROM q_pools p
                              WHERE p.rowid = ${req.body.id}`;
                let resultCounts = await db.sequelize.query(queryCounts);

                let resultResponse = null;
                if (resultCounts[0][0].countEliminatorias === 0) {
                    // evaluar puntaje de cada equipo por grupo
                    resultResponse = await axios({
                        method: "get",
                        url: `${process.env.BACKEND_URL}/api/result/getRankingForPool?poolId=${req.body.id}`,
                        responseType: "appilcation/json"
                    })

                    console.log(JSON.stringify(resultResponse.data));
                }

                // creacion de 8vos
                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countOctavosMatches === 0) {
                    // crear los partidos B1 con A2, A1 con B2, D1 con C2, C1 con D2, F1 con E2, E1 con F2, H1 con G2 y G1 con H2
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                                   result_team_1_penales_, fk_team_2, result_team_2,
                                                                   result_team_2_penales_, date_Sport, hour, status,
                                                                   result, number_pools, label, penalty, bracket_type)
                                      VALUES (${req.body.id},
                                              ${resultResponse.data.groups[1].results[0].teamId},
                                              0,
                                              0, 
                                              ${resultResponse.data.groups[0].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              1,
                                              '${resultResponse.data.groups[1].results[0].name + ' vs ' + resultResponse.data.groups[0].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id},
                                              ${resultResponse.data.groups[0].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[1].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              2,
                                              '${resultResponse.data.groups[0].results[0].name + ' vs ' + resultResponse.data.groups[1].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[3].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[2].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              3,
                                              '${resultResponse.data.groups[3].results[0].name + ' vs ' + resultResponse.data.groups[2].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[2].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[3].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              4,
                                              '${resultResponse.data.groups[2].results[0].name + ' vs ' + resultResponse.data.groups[3].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[5].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[4].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              5,
                                              '${resultResponse.data.groups[5].results[0].name + ' vs ' + resultResponse.data.groups[4].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[4].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[5].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              6,
                                              '${resultResponse.data.groups[4].results[0].name + ' vs ' + resultResponse.data.groups[5].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id},
                                              ${resultResponse.data.groups[7].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[6].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              7,
                                              '${resultResponse.data.groups[7].results[0].name + ' vs ' + resultResponse.data.groups[6].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')
                                              ,
                                              (${req.body.id}, 
                                              ${resultResponse.data.groups[6].results[0].teamId},
                                              0,
                                              0,
                                              ${resultResponse.data.groups[7].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              8,
                                              '${resultResponse.data.groups[6].results[0].name + ' vs ' + resultResponse.data.groups[7].results[1].name}', 
                                              ${req.body.penalty},
                                              'Octavos de Final')`;
                    let resultMatch = await db.sequelize.query(queryMatch);

                }

                // creacion de 4tos
                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countOctavos === 0 && resultCounts[0][0].countCuartosMatches === 0) {
                    // crear los partidos
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                                   result_team_1_penales_, fk_team_2, result_team_2,
                                                                   result_team_2_penales_, date_Sport, hour, status,
                                                                   result, number_pools, label, penalty, bracket_type)
                                      VALUES (${req.body.id},
                                              ${getResultTeamByBracket(resultResponse.data.brackets[0]).teamId},
                                              0,
                                              0,
                                              ${getResultTeamByBracket(resultResponse.data.brackets[1]).teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C',
                                              1,
                                              '${getResultTeamByBracket(resultResponse.data.brackets[0]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[1]).name}
                                          ',
                                              ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id},
                                               ${getResultTeamByBracket(resultResponse.data.brackets[2]).teamId},
                                               0,
                                               0,
                                               ${getResultTeamByBracket(resultResponse.data.brackets[3]).teamId},
                                               0,
                                               0,
                                               '${moment().format('YYYY-MM-DD')}',
                                               '11:00 am',
                                               1,
                                               'C',
                                               2,
                                               '${getResultTeamByBracket(resultResponse.data.brackets[2]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[3]).name}
                                          ',
                                               ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id},
                                               ${getResultTeamByBracket(resultResponse.data.brackets[4]).teamId},
                                              0,
                                              0,
                                               ${getResultTeamByBracket(resultResponse.data.brackets[5]).teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              3,
                                               '${getResultTeamByBracket(resultResponse.data.brackets[4]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[5]).name}
                                          ',
                                               ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id},
                                               ${getResultTeamByBracket(resultResponse.data.brackets[6]).teamId},
                                               0,
                                               0,
                                               ${getResultTeamByBracket(resultResponse.data.brackets[7]).teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              4,
                                               '${getResultTeamByBracket(resultResponse.data.brackets[6]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[7]).name}
                                          ',
                                               ${req.body.penalty},
                                              'Cuartos de Final')`;
                    let resultMatch = await db.sequelize.query(queryMatch);

                }

                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countOctavos === 0 && resultCounts[0][0].countCuartos === 0 && resultCounts[0][0].countSemiFinalMatches === 0) {
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status,
                                                               result, number_pools, label, penalty, bracket_type)
                                  VALUES (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[8]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[9]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          1,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[8]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[9]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Semifinal')
                                          ,
                                         (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[10]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[11]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          2,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[10]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[11]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Semifinal')`;
                    let resultMatch = await db.sequelize.query(queryMatch);
                }

                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countOctavos === 0 && resultCounts[0][0].countCuartos === 0 && resultCounts[0][0].countSemiFinal === 0 && resultCounts[0][0].countFinalMatches === 0) {
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status,
                                                               result, number_pools, label, penalty, bracket_type)
                                  VALUES (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[12]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[13]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          1,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[12]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[13]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Final')
                                          ,
                                         (${req.body.id},
                                          ${getResultTeamByBracketReverse(resultResponse.data.brackets[12]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracketReverse(resultResponse.data.brackets[13]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          2,
                                          '${getResultTeamByBracketReverse(resultResponse.data.brackets[12]).name + ' vs ' + getResultTeamByBracketReverse(resultResponse.data.brackets[13]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Mejor Tercero')`;
                    let resultMatch = await db.sequelize.query(queryMatch);
                }

            } else {
                //verificar si las eliminatorias se cerraron
                let queryCounts = `SELECT
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type is null) As countEliminatorias,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Cuartos de Final') As countCuartos,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid AND bracket_type = 'Cuartos de Final') As countCuartosMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Semifinal') As countSemiFinal,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and bracket_type = 'Semifinal') As countSemiFinalMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Final') As countFinal,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and bracket_type = 'Final') As countFinalMatches,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = 'C' AND bracket_type = 'Mejor Tercero') As countMejorTercero,
                                  (SELECT count(*)  FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and  bracket_type = 'Mejor Tercero') As countMejorTerceroMatches
                              FROM q_pools p
                              WHERE p.rowid = ${req.body.id}`;
                let resultCounts = await db.sequelize.query(queryCounts);

                let resultResponse = null;
                if (resultCounts[0][0].countEliminatorias === 0) {
                    // evaluar puntaje de cada equipo por grupo
                    resultResponse = await axios({
                        method: "get",
                        url: `${process.env.BACKEND_URL}/api/result/getRankingForPool?poolId=${req.body.id}`,
                        responseType: "appilcation/json"
                    })

                    console.log(JSON.stringify(resultResponse.data));
                }

                // creacion de 4tos
                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countCuartosMatches === 0) {
                    // crear los partidos B1 con A2, A1 con B2, D1 con C2, C1 con D2
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                                   result_team_1_penales_, fk_team_2, result_team_2,
                                                                   result_team_2_penales_, date_Sport, hour, status,
                                                                   result, number_pools, label, penalty, bracket_type)
                                      VALUES (${req.body.id}, 
                                              ${resultResponse.data.groups[1].results[0].teamId},
                                              0,
                                              0, 
                                              ${resultResponse.data.groups[0].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              1,
                                              '${resultResponse.data.groups[1].results[0].name + ' vs ' + resultResponse.data.groups[0].results[1].name}', 
                                              ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id},
                                               ${resultResponse.data.groups[0].results[0].teamId},
                                               0,
                                               0,
                                               ${resultResponse.data.groups[1].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              2,
                                               '${resultResponse.data.groups[0].results[0].name + ' vs ' + resultResponse.data.groups[1].results[1].name}',
                                               ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id},
                                               ${resultResponse.data.groups[3].results[0].teamId},
                                               0,
                                               0,
                                               ${resultResponse.data.groups[2].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              3,
                                               '${resultResponse.data.groups[3].results[0].name + ' vs ' + resultResponse.data.groups[2].results[1].name}',
                                               ${req.body.penalty},
                                              'Cuartos de Final')
                                              ,
                                              (${req.body.id},
                                               ${resultResponse.data.groups[2].results[0].teamId},
                                               0,
                                               0,
                                               ${resultResponse.data.groups[3].results[1].teamId},
                                              0,
                                              0,
                                              '${moment().format('YYYY-MM-DD')}',
                                              '11:00 am',
                                              1,
                                              'C', 
                                              4,
                                               '${resultResponse.data.groups[2].results[0].name + ' vs ' + resultResponse.data.groups[3].results[1].name}',
                                               ${req.body.penalty},
                                              'Cuartos de Final')`;
                    let resultMatch = await db.sequelize.query(queryMatch);

                }

                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countCuartos === 0 && resultCounts[0][0].countSemiFinalMatches === 0) {
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status,
                                                               result, number_pools, label, penalty, bracket_type)
                                  VALUES (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[0]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[1]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          1,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[0]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[1]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Semifinal')
                                          ,
                                         (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[2]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[3]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          2,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[2]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[3]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Semifinal')`;
                    let resultMatch = await db.sequelize.query(queryMatch);
                }

                if (resultCounts[0][0].countEliminatorias === 0 && resultCounts[0][0].countCuartos === 0 && resultCounts[0][0].countSemiFinal === 0 && resultCounts[0][0].countFinalMatches === 0) {
                    let queryMatch = `INSERT INTO q_pools_details (fk_pools, fk_team_1, result_team_1,
                                                               result_team_1_penales_, fk_team_2, result_team_2,
                                                               result_team_2_penales_, date_Sport, hour, status,
                                                               result, number_pools, label, penalty, bracket_type)
                                  VALUES (${req.body.id},
                                          ${getResultTeamByBracket(resultResponse.data.brackets[4]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracket(resultResponse.data.brackets[5]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          1,
                                          '${getResultTeamByBracket(resultResponse.data.brackets[4]).name + ' vs ' + getResultTeamByBracket(resultResponse.data.brackets[5]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Final')
                                          ,
                                         (${req.body.id},
                                          ${getResultTeamByBracketReverse(resultResponse.data.brackets[4]).teamId},
                                          0,
                                          0,
                                          ${getResultTeamByBracketReverse(resultResponse.data.brackets[5]).teamId},
                                          0,
                                          0,
                                          '${moment().format('YYYY-MM-DD')}',
                                          '11:00 am',
                                          1,
                                          'C',
                                          2,
                                          '${getResultTeamByBracketReverse(resultResponse.data.brackets[2]).name + ' vs ' + getResultTeamByBracketReverse(resultResponse.data.brackets[3]).name}
                                          ',
                                          ${req.body.penalty},
                                          'Mejor Tercero')`;
                    let resultMatch = await db.sequelize.query(queryMatch);
                }

            }
        }

        //transaction, verificamos que la quiniela esta en condiciones de cerrado
        let queryPools = `SELECT p.status,
                                 p.awardDelivered,
                                 (SELECT count(*)
                                  FROM q_pools_details t2
                                  WHERE t2.fk_pools = p.rowid and t2.result = 'C') As countMatch,
                                 (SELECT COUNT(*) as finishDate
                                  FROM q_pools
                                  WHERE dateFinish is not null
                                    AND STR_TO_DATE(CONCAT(dateFinish, ' ', timeFinish), '%Y-%m-%d %H:%i:%s') >= NOW()
                                    AND p.rowid = ${req.body.id})                  as dateFinish
                          FROM q_pools p
                          WHERE p.rowid = ${req.body.id}`;
        let resultPools = await db.sequelize.query(queryPools);

        //transaction, creamos la transaccion y actualizamos la quiniela
        console.log(JSON.stringify(resultPools[0][0]));
        if (resultPools[0][0].status === 2 && resultPools[0][0].awardDelivered === 0 && resultPools[0][0].countMatch === 0) {
            // actualizamos quiniela
            let queryUpdatePoolAward = `UPDATE q_pools
                                        SET awardDelivered = 1
                                        WHERE rowid = ${req.body.id}`;
            let resultUpdatePoolAward = await db.sequelize.query(queryUpdatePoolAward);

            // buscamos al usuario con mayor puntaje en la quiniela
            let resultResponse = await axios({
                method: "get",
                url: `${process.env.BACKEND_URL}/api/result/getResultsByPool?poolId=${req.body.id}`,
                responseType: "appilcation/json"
            })

            console.log(JSON.stringify(resultResponse.data));

            if (resultResponse.data.data.length > 0) {
                let userId = resultResponse.data.data[0].userID;

                let user = await db.q_user.findOne({
                    where: {rowid: {[Op.eq]: userId}}
                });

                let pool = await db.q_pools.findOne({
                    where: {rowid: {[Op.eq]: req.body.id}}
                });

                user.dataValues.amount += pool.dataValues.awardValue;

                let dataUserUpdate = await db.q_user.update(user.dataValues, {where: {rowid: userId}});

                const dataTransaction = await db.q_transaction.create({
                    username: user.dataValues.username,
                    amount: pool.dataValues.awardValue,
                    coins: 0,
                    description: 'El usuario ' + user.dataValues.username + ' ha Ganado el premio de la quiniela ' + pool.dataValues.name
                });
            }
        }

        res.send({
            data: resultPool,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getOne = async (req, res) => {

    try {
        let pool = await db.q_pools.findOne({
            where: {rowid: {[Op.eq]: req.query.poolId}}
        });

        let data = {
            id: pool.dataValues.rowid,
            rules: pool.dataValues.rules,
            name: pool.dataValues.name,
            sport: pool.dataValues.fk_sport,
            color: pool.dataValues.color,
            matches: pool.dataValues.quantity,
            usersLimit: pool.dataValues.limit_user,
            status: pool.dataValues.status,
            penalty: pool.dataValues.penalty,
            groups: pool.dataValues.groups,
            teamsPerGroup: pool.dataValues.groupsTeam,
            type: pool.dataValues.pool_type,
            league: pool.dataValues.league,
            tournamentType: pool.dataValues.tournament_type,
            hot: pool.dataValues.hot,
            password: pool.dataValues.password,
            result: pool.dataValues.puntaje_resultado,
            winner: pool.dataValues.puntaje_ganar,
            draw: pool.dataValues.puntaje_empate,
            loser: pool.dataValues.puntaje_perder,
            amountInput: pool.dataValues.amountInput,
            coinsInput: pool.dataValues.coinsInput,
            dateFinish: pool.dataValues.dateFinish,
            timeFinish: pool.dataValues.timeFinish,
            awardType: pool.dataValues.awardType,
            awardValue: pool.dataValues.awardValue,
        };

        let matchesInfo = [];
        let usersForPool = [];

        //get matches
        let queryMatches = 'SELECT q.*, qt1.name as team1 , qt2.name as team2, qt1.img as image1, qt2.img as image2 FROM q_pools_details q left join q_team qt1 on q.fk_team_1 = qt1.rowid left join q_team qt2 on q.fk_team_2 = qt2.rowid where fk_pools = ' + req.query.poolId;
        let resultMatches = await db.sequelize.query(queryMatches);

        resultMatches[0].forEach(match => {
            matchesInfo.push({
                id: match.rowid,
                title: match.label,
                team1: match.fk_team_1,
                teamName1: match.team1,
                penalty1: match.result_team_1_penales_,
                result1: match.result_team_1,
                image1: match.image1,
                team2: match.fk_team_2,
                teamName2: match.team2,
                penalty2: match.result_team_2_penales_,
                result2: match.result_team_2,
                image2: match.image2,
                date: match.date_Sport,
                time: match.hour,
                status: match.status,
                result: match.result,
                bracketType: match.bracket_type,
            })
        });

        data.matchesInfo = matchesInfo;

        let queryUsers = 'SELECT * FROM q_user_pools where fk_q_pools = ' + req.query.poolId;
        let resultUsers = await db.sequelize.query(queryUsers);

        resultUsers[0].forEach(user => {
            usersForPool.push(user.fk_q_user);
        });

        let queryGroups = 'SELECT * FROM q_groups q where poolId = ' + req.query.poolId;
        let resultGroups = await db.sequelize.query(queryGroups);

        let groups = [];


        const forLoop = async _ => {
            for (let index = 0; index < resultGroups[0].length; index++) {

                let queryGroupsTeams = 'SELECT * FROM q_group_team q where groupId = ' + resultGroups[0][index].rowid;
                let resultGroupsTeams = await db.sequelize.query(queryGroupsTeams);

                groups.push({
                    name: resultGroups[0][index].name,
                    poolId: req.query.poolId,
                    teams: resultGroupsTeams[0].map(x => x.teamId)
                })
            }
        };

        await forLoop();

        data.usersForPool = usersForPool;
        data.groupsInfo = groups;

        res.send({
            data: data,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getPoolsByUser = async (req, res) => {

    try {
        let queryUsers = 'SELECT fk_q_pools FROM q_user_pools where fk_q_user = ' + req.query.userId;
        let resultUsers = await db.sequelize.query(queryUsers);

        res.send({
            data: resultUsers[0],
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getMatchesByPoolAndUser = async (req, res) => {

    try {
        let queryUsers = 'SELECT fk_q_pools FROM q_user_pools where fk_q_user = ' + req.query.userId;
        let resultUsers = await db.sequelize.query(queryUsers);

        res.send({
            data: resultUsers[0],
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};


exports.getMyPools = async (req, res) => {

    try {
        // ubicamos los valores que necesitamos de las quinielas asociadas al usuario
        let queryPools = 'SELECT DISTINCT p.*, (  SELECT count(*) FROM q_user_pools t1 WHERE t1.fk_q_pools = qup.fk_q_pools) As countUsers, (  SELECT count(*) FROM q_pools_details t2 WHERE t2.fk_pools = qup.fk_q_pools and t2.result = \'C\') As countMatch FROM q_pools p left join q_user_pools qup on p.rowid = qup.fk_q_pools WHERE qup.fk_q_user = ' + req.query.userId;
        let resultPools = await db.sequelize.query(queryPools);

        // ajustamos la data
        let poolsRegistered = [];
        let poolsInProgress = [];
        let poolsHistorical = [];

        resultPools[0].forEach(pool => {

            let date = null;
            if (pool.dateFinish !== null && pool.timeFinish !== null) {
                date = pool.dateFinish + ' ' + pool.timeFinish;
            }

            if (pool.status === 1) {
                poolsRegistered.push({
                    id: pool.rowid,
                    name: pool.name,
                    amountInput: pool.amountInput,
                    coinsInput: pool.coinsInput,
                    awardValue: pool.awardValue,
                    participants: pool.countUsers + '/' + pool.limit_user,
                    timeRemaining: date
                });
            } else {
                if (pool.countMatch > 0) {
                    poolsInProgress.push({
                        id: pool.rowid,
                        name: pool.name,
                        amountInput: pool.amountInput,
                        coinsInput: pool.coinsInput,
                        awardValue: pool.awardValue,
                        participants: pool.countUsers + '/' + pool.limit_user,
                        timeRemaining: date
                    });
                } else {
                    poolsHistorical.push({
                        id: pool.rowid,
                        name: pool.name,
                        amountInput: pool.amountInput,
                        coinsInput: pool.coinsInput,
                        awardValue: pool.awardValue,
                        participants: pool.countUsers + '/' + pool.limit_user,
                        timeRemaining: date
                    });
                }
            }
        });


        res.send({
            registered: poolsRegistered,
            progress: poolsInProgress,
            historical: poolsHistorical,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};


exports.getMyHotPools = async (req, res) => {

    function getStatus(pool) {
        if (pool.status === 1) {
            return "registered";
        }
        if (pool.countMatch > 0) {
            return "progress";
        } else {
            return "historical";
        }
    }

    try {
        // ubicamos los valores que necesitamos de las quinielas asociadas al usuario
        let queryIndexPools = `
            SELECT DISTINCT p.rowid
            FROM q_pools p
                     left join q_user_pools qup on p.rowid = qup.fk_q_pools
            WHERE p.hot = 1
        `;
        let resultIndexPools = await db.sequelize.query(queryIndexPools);

        let queryPools = `
            SELECT DISTINCT p.*,
                    (SELECT count(*) FROM q_user_pools t1 WHERE t1.fk_q_pools = qup.fk_q_pools)                      As countUsers,
                    (SELECT count(*)
                     FROM q_pools_details t2
                     WHERE t2.fk_pools = qup.fk_q_pools
                       and t2.result = 'C')                                                                         As countMatch,
                    qup.fk_q_user = ${req.query.userId} As registered
            FROM q_pools p
                     left join q_user_pools qup on p.rowid = qup.fk_q_pools
            WHERE p.hot = 1
        `;
        let resultPools = await db.sequelize.query(queryPools);

        // ajustamos la data
        let pools = [];

        resultIndexPools[0].forEach(poolIndex => {

            //limpiamos los duplicados falsos
            let poolResults = resultPools[0].filter(x => x.rowid === poolIndex.rowid);

            let pool = null;
            if (poolResults.length > 1) {
                //tomamos el valor donde el usuario esta registrado a la quiniela
                pool = poolResults.find(x => x.registered === 1)
            } else {
                pool = poolResults[0];
            }

            let date = null;
            if (pool.dateFinish !== null && pool.timeFinish !== null) {
                date = pool.dateFinish + ' ' + pool.timeFinish;
            }

            let status = getStatus(pool);

            pools.push({
                id: pool.rowid,
                name: pool.name,
                amountInput: pool.amountInput,
                coinsInput: pool.coinsInput,
                awardValue: pool.awardValue,
                status: status,
                password: pool.password,
                rules: pool.rules !== null ? pool.rules : "",
                registered: pool.registered === 1,
                participants: pool.countUsers + '/' + pool.limit_user,
                timeRemaining: date
            });
        });

        res.send({
            pools: pools,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};


exports.getPoolsForAdmin = async (req, res) => {

    try {
        // ubicamos los valores que necesitamos de las quinielas asociadas al usuario
        let queryPools = 'SELECT p.*,\n' +
            '       s.name as sport,\n' +
            '       (select count(*) from q_pools_details where fk_pools = p.rowid) as matches,\n' +
            '       (  SELECT count(*) FROM q_pools_details t2 WHERE t2.fk_pools = p.rowid and t2.result = \'C\') As countMatch\n' +
            'FROM q_pools p\n' +
            '    left join q_sport s on p.fk_sport = s.rowid';
        let resultPools = await db.sequelize.query(queryPools);

        // ajustamos la data
        let pools = [];

        resultPools[0].forEach(pool => {

            let date = null;
            if (pool.dateFinish !== null && pool.timeFinish !== null) {
                date = pool.dateFinish + ' ' + pool.timeFinish;
            }

            if (pool.status === 1) {
                //for start
                pools.push({
                    id: pool.rowid,
                    name: pool.name,
                    sport: pool.sport,
                    matches: pool.matches,
                    result: 'FOR START',
                    timeRemaining: date
                });
            } else {
                // Quinielas en proceso
                if (pool.countMatch > 0) {
                    pools.push({
                        id: pool.rowid,
                        name: pool.name,
                        sport: pool.sport,
                        matches: pool.matches,
                        result: 'IN PROCESS',
                        timeRemaining: date
                    });
                } else {
                    //finished
                    pools.push({
                        id: pool.rowid,
                        name: pool.name,
                        sport: pool.sport,
                        matches: pool.matches,
                        result: 'FINISHED',
                        timeRemaining: date
                    });
                }
            }
        });


        res.send({
            pools: pools,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};


exports.getMyAvailablePools = async (req, res) => {

    try {
        // ubicamos los valores que necesitamos de las quinielas asociadas al usuario
        let queryPools = `SELECT DISTINCT p.*, sport.name as sport, (  SELECT count(*) FROM q_user_pools t1 WHERE t1.fk_q_pools = qup.fk_q_pools) As countUsers FROM q_pools p left join q_user_pools qup on p.rowid = qup.fk_q_pools left join q_sport sport on p.fk_sport = sport.rowid WHERE p.status = 1 AND p.league = '${req.query.poolCategory}'`;
        let resultPools = await db.sequelize.query(queryPools);

        let queryUserPools = 'SELECT p.fk_q_pools FROM q_user_pools p WHERE p.fk_q_user = ' + req.query.userId;
        let resultUserPools = await db.sequelize.query(queryUserPools);

        // ajustamos la data
        let pools1VS1 = [];
        let poolsMensuales = [];
        let poolsSemanales = [];
        let poolsPrivadas = [];

        resultPools[0].forEach(pool => {
            // if (pool.limit_user > pool.countUsers) {
            let date = null;
            if (pool.dateFinish !== null && pool.timeFinish !== null) {
                date = pool.dateFinish + ' ' + pool.timeFinish;
            }

            const item = {
                id: pool.rowid,
                name: pool.name,
                sport: pool.sport,
                status: pool.status,
                date: pool.date_Create,
                timeRemaining: date,
                amountInput: pool.amountInput,
                coinsInput: pool.coinsInput,
                awardValue: pool.awardValue,
                password: pool.password,
                rules: pool.rules,
                participants: pool.countUsers + '/' + pool.limit_user,
                registered: resultUserPools[0].map(x => x.fk_q_pools).includes(pool.rowid)
            };

            if (pool.pool_type === '1 vs 1') {
                pools1VS1.push(item);
            } else if (pool.pool_type === 'mensuales') {
                poolsMensuales.push(item);
            } else if (pool.pool_type === 'semanales') {
                poolsSemanales.push(item);
            } else if (pool.pool_type === 'privadas') {
                poolsPrivadas.push(item);
            }
            // }
        });


        res.send({
            oneVSone: pools1VS1,
            mensuales: poolsMensuales,
            semanales: poolsSemanales,
            privadas: poolsPrivadas,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getUsersByPool = async (req, res) => {

    try {
        let queryUsers = 'SELECT * FROM q_user left join q_user_pools qup on q_user.rowid = qup.fk_q_user where qup.fk_q_pools = ' + req.query.poolId;
        let resultUsers = await db.sequelize.query(queryUsers);

        res.send({
            data: resultUsers[0],
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getLinkByPool = async (req, res) => {

    try {
        let buff = new Buffer(req.query.poolId);
        let base64data = buff.toString('base64');

        res.send({
            link: process.env.URL_FRONTEND + '/share?code=' + base64data,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.poolId} .`,
        });
    }
};

exports.registerUserPoolByLinkCode = async (req, res) => {
        let error = false;
    try {

        //extraemos el poolId
        let buff = new Buffer(req.body.code, 'base64');
        let poolId = buff.toString('ascii');

        // transaction
        let pool = await db.q_pools.findOne({
            where: {rowid: {[Op.eq]: poolId}}
        });

        let user = await db.q_user.findOne({
            where: {rowid: {[Op.eq]: req.body.userId}}
        });

        //validamos el estatus
        if (pool.dataValues.status !== 1) {
            error = true;
            res.status(200).send({
                code: 'D402',
                message: 'La quiniela esta en proceso o finalizada!',
            });
        }

        if (!error) {
            //validamos el limite de personas de la quiniela
            let queryPools = 'SELECT * FROM q_user_pools qup  where qup.fk_q_pools = ' + poolId;
            let resultPools = await db.sequelize.query(queryPools);

            if (resultPools[0].length >= pool.dataValues.limit_user) {
                error = true;
                res.status(200).send({
                    code: 'D402',
                    message: 'Se ha superado el limite de la quiniela',
                });
            }
        }

        if (!error) {
            //validamos que no estes en la quiniela ya
            let queryPools = 'SELECT * FROM q_user_pools qup  where qup.fk_q_user = ' + req.body.userId + ' AND qup.fk_q_pools = ' + poolId;
            let resultPools = await db.sequelize.query(queryPools);

            if (resultPools[0].length > 0) {
                error = true;
                res.status(200).send({
                    code: 'D402',
                    message: 'Usted esta registrado en la quiniela',
                });
            }
        }

        if (!error) {
            if (user.dataValues.coins >= pool.dataValues.coinsInput) {
                if (user.dataValues.amount >= pool.dataValues.amountInput) {
                    user.dataValues.coins -= pool.dataValues.coinsInput;
                    user.dataValues.amount -= pool.dataValues.amountInput;

                    let dataUserUpdate = await db.q_user.update(user.dataValues, {where: {rowid: req.body.userId}});

                    if (pool.dataValues.awardType === 'total') {
                        pool.dataValues.awardValue += pool.dataValues.amountInput
                        let dataPoolUpdate = await db.q_pools.update(pool.dataValues, {where: {rowid: poolId}});
                    }

                    const dataTransaction = await db.q_transaction.create({
                        username: user.dataValues.username,
                        amount: pool.dataValues.amountInput,
                        coins: pool.dataValues.coinsInput,
                        description: 'El usuario ' + user.dataValues.username + ' ha ingresado a la quiniela ' + pool.dataValues.name
                    });

                    let queryUser = `INSERT INTO q_user_pools (fk_q_user, fk_q_pools)
                                 VALUES (${req.body.userId}, ${poolId})`;
                    let resultUser = await db.sequelize.query(queryUser);

                    res.send({
                        data: resultUser,
                        code: 'D200',
                    });

                } else {
                    res.status(200).send({
                        code: 'D402',
                        message: 'El cliente no posee la cantidad necesaria de monto para ingresar!',
                    });
                }
            } else {
                res.status(200).send({
                    code: 'D401',
                    message: 'El cliente no posee la cantidad necesaria de glucks para ingresar!',
                });
            }
        }
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getPoolInfo = async (req, res) => {
    let error = false;

    try {
        //extraemos el poolId
        let buff = new Buffer(req.body.code, 'base64');
        let poolId = buff.toString('ascii');

        // transaction
        let pool = await db.q_pools.findOne({
            where: {rowid: {[Op.eq]: poolId}}
        });

        let user = await db.q_user.findOne({
            where: {rowid: {[Op.eq]: req.body.userId}}
        });

        //validamos el estatus
        if (pool.dataValues.status !== 1) {
            error = true;
            res.status(200).send({
                code: 'D402',
                message: 'La quiniela esta en proceso o finalizada!',
            });
        }

        if (!error) {
            //validamos el limite de personas de la quiniela
            let queryPools = 'SELECT * FROM q_user_pools qup  where qup.fk_q_pools = ' + poolId;
            let resultPools = await db.sequelize.query(queryPools);

            if (resultPools[0].length >= pool.dataValues.limit_user) {
                error = true;
                res.status(200).send({
                    code: 'D402',
                    message: 'Se ha superado el limite de la quiniela',
                });
            }
        }

        if (!error) {
            //validamos que no estes en la quiniela ya
            let queryPools = 'SELECT * FROM q_user_pools qup  where qup.fk_q_user = ' + req.body.userId + ' AND qup.fk_q_pools = ' + poolId;
            let resultPools = await db.sequelize.query(queryPools);

            if (resultPools[0].length > 0) {
                error = true;
                res.status(200).send({
                    code: 'D402',
                    message: 'Usted esta registrado en la quiniela',
                });
            }
        }

        if (!error) {
            // ubicamos los valores que necesitamos de las quinielas asociadas al usuario
            let queryPools = `SELECT DISTINCT p.*, sport.name as sport, (  SELECT count(*) FROM q_user_pools t1 WHERE t1.fk_q_pools = qup.fk_q_pools) As countUsers FROM q_pools p left join q_user_pools qup on p.rowid = qup.fk_q_pools left join q_sport sport on p.fk_sport = sport.rowid WHERE p.status = 1 AND p.rowid = '${poolId}'`;
            let resultPools = await db.sequelize.query(queryPools);

            let queryUserPools = 'SELECT p.fk_q_pools FROM q_user_pools p WHERE p.fk_q_user = ' + req.body.userId;
            let resultUserPools = await db.sequelize.query(queryUserPools);

            // ajustamos la data
            let pools = [];

            resultPools[0].forEach(pool => {
                // if (pool.limit_user > pool.countUsers) {
                let date = null;
                if (pool.dateFinish !== null && pool.timeFinish !== null) {
                    date = pool.dateFinish + ' ' + pool.timeFinish;
                }

                const item = {
                    id: pool.rowid,
                    name: pool.name,
                    sport: pool.sport,
                    status: pool.status,
                    date: pool.date_Create,
                    timeRemaining: date,
                    amountInput: pool.amountInput,
                    coinsInput: pool.coinsInput,
                    awardValue: pool.awardValue,
                    password: pool.password,
                    rules: pool.rules,
                    participants: pool.countUsers + '/' + pool.limit_user,
                    registered: resultUserPools[0].map(x => x.fk_q_pools).includes(pool.rowid)
                };

                pools.push(item);
                // }
            });

            res.send({
                pools: pools,
                code: 'D200',
            });
        }
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};

exports.getDashboardPool = async (req, res) => {

    try {
        let queryPools = 'SELECT * FROM q_user left join q_user_pools qup on q_user.rowid = qup.fk_q_user where qup.fk_q_pools = ' + req.query.poolId;
        let resultPools = await db.sequelize.query(queryPools);

        res.send({
            data: resultPools[0],
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};



exports.updatePoolStatusToInProcess = async (req, res) => {

    try {
        //update Pool status
        let queryPool = `UPDATE q_pools
                         SET 
                             status            = 2
                         WHERE rowid = ${req.body.poolId}`;
        let resultPool = await db.sequelize.query(queryPool);

        res.send({
            data: resultPool,
            code: 'D200',
        });
    } catch (err) {
        res.status(200).send({
            code: 'D500',
            message:
                err.message ||
                `Some error occurred while retrieving id = ${req.query.id} .`,
        });
    }
};
