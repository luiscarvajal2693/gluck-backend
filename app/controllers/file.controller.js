const fs = require('fs');
const uuid = require('uuid');
const path = require('path');

var im = require('imagemagick');

class FileController {

    async uploadFile(req, res) {
        try {

            let filename = null;

            if (req.body.imageData) {
                const type = '.png';
                filename = uuid.v4() + type;
                const filePath = path.join(appRoot, '/public/images/' + filename);

                const fileContents = new Buffer(req.body.imageData.split(';base64,').pop(), 'base64');
                fs.writeFile(filePath, fileContents, (err) => {
                    if (err) return console.error(err);
                    console.log('file saved to ', filename)
                });
            }

            await res.json(filename);
        } catch (error) {
            res.status(500);
            res.send(error.message)
        }
    }

    async removeFile(req, res) {
        try {
            if (req.body.id) {
                let filename = req.body.id;
                const filePath = path.join(appRoot, '/public/images/' + filename);
                await fs.unlinkSync(filePath);
                await res.json(true);
            } else {
                await res.json(false);
            }
        } catch (error) {
            res.status(500);
            res.send(error.message)
        }
    }

    async getFile(req, res) {
        async function compressImage(filePath, filePathSmall) {
            return new Promise( (resolve, reject) => {
                im.resize({
                        srcPath: filePath,
                        dstPath: filePathSmall,
                        width:   256
                    }, function(err, stdout){
                    if(err) {
                        reject(err)
                    }
                    resolve(stdout);
                });
            });
        }

        try {
            if (req.query.id) {
                let filename = req.query.id;
                const filePath = path.join(appRoot, '/public/images/' + filename);

                    let result;
                    if (req.query.isSmall) {
                        const filePathSmall = path.join(appRoot, '/public/images/' + filename.replace('.png', '') + '-small.png');
                        let compressImageResult = await compressImage(filePath, filePathSmall);
                        result = await fs.readFileSync(filePathSmall);
                    } else {
                        result = await fs.readFileSync(filePath);
                    }
                    await res.json( 'data:image/png;base64,' + result.toString('base64'));
            } else {
                await res.json(null);
            }
        } catch (error) {
            res.status(500);
            res.send(error.message)
        }
    }

}

const controller = new FileController();
module.exports = controller;
