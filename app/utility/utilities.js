const nodemailer = require("nodemailer");
const fs = require("fs");
const Handlebars = require("handlebars");

const path = require('path');

exports.sendEmailData = (reqId, toEmail, subject, templateName, paramsEmail, withLogo) => {
  return new Promise(async (resolve, reject) => {
    let transporter = null;

    transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      }
    });

    const templatePath = path.join(
        appRoot,
        '/public/emails/templates/',
        templateName + ".html");

    try {
      let source = await fs.readFileSync(templatePath);
      let template = Handlebars.compile(source.toString());
      template = template(paramsEmail);

      let resultArray = [];
      let mailOptions = {}

      mailOptions = {
        from: process.env.SMTP_USERNAME,
        to: toEmail,
        subject: subject,
        html: template
      };

      let result = await transporter.sendMail(mailOptions);

      resultArray.push({
        result: "Message sent: " + result.messageId
      });

      resolve(resultArray);
    } catch (err) {
      console.log(err);
      reject({
        result: "Error",
        error: err
      });
    }
  });
}

exports.sendEmailDataWithoutTemplate = (reqId, toEmail, subject, body) => {
  return new Promise(async (resolve, reject) => {
    let transporter = null;

    transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      }
    });


    try {

      let resultArray = [];

      mailOptions = {
        from: process.env.SMTP_USERNAME,
        to: toEmail,
        subject: subject,
        html: body
      };

      let result = await transporter.sendMail(mailOptions);

      resultArray.push({
        result: "Message sent: " + result.messageId
      });

      resolve(resultArray);
    } catch (err) {
      console.log(err);
      reject({
        result: "Error",
        error: err
      });
    }
  });
}
