module.exports = (app) => {
  const poolController = require('../controllers/pool.controller');

  var router = require('express').Router();

  router.post('/create' , poolController.create);
  router.post('/update' , poolController.update);
  router.post('/updatePoolStatusToInProcess' , poolController.updatePoolStatusToInProcess);
  router.post('/registerUserPool' , poolController.registerUserPool);
  router.post('/unRegisterUserPool' , poolController.unRegisterUserPool);
  router.post('/registerUserPoolByLinkCode' , poolController.registerUserPoolByLinkCode);
  router.post('/getPoolInfo' , poolController.getPoolInfo);
  router.get('/getOne' , poolController.getOne);
  router.get('/getMyPools' , poolController.getMyPools);
  router.get('/getMyHotPools' , poolController.getMyHotPools);
  router.get('/getPoolsForAdmin' , poolController.getPoolsForAdmin);
  router.get('/getMyAvailablePools' , poolController.getMyAvailablePools);
  router.get('/getPoolsByUser' , poolController.getPoolsByUser);
  router.get('/getUsersByPool' , poolController.getUsersByPool);
  router.get('/getLinkByPool' , poolController.getLinkByPool);
  router.get('/getDashboardPool' , poolController.getDashboardPool);

  app.use('/api/pool', router);
};
