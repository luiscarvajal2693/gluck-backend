module.exports = (app) => {
  const userController = require('../controllers/user.controller');

  var router = require('express').Router();

  router.post('/rechargue' , userController.rechargue);

  app.use('/api/user', router);
};
