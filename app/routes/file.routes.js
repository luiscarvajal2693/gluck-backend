module.exports = (app) => {
  const fileController = require('../controllers/file.controller');

  var router = require('express').Router();

  router.post('/upload' , fileController.uploadFile);
  router.post('/remove' , fileController.removeFile);
  router.get('/get' , fileController.getFile);

  app.use('/api/file', router);
};
