module.exports = (app) => {
  const utility = require('../controllers/utilities.controller');

  var router = require('express').Router();

  router.get('/getVersion', utility.getVersion);
  router.post('/sendEmail', utility.sendEmail);
  router.post('/sendEmailByCategory', utility.sendEmailByCategory);

  app.use('/api/utilities', router);
};
