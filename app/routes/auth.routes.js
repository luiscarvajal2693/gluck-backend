module.exports = (app) => {
  const auth = require('../controllers/auth.controller');

  var router = require('express').Router();

  router.post('/login', auth.login);
  router.post('/register', auth.signup);
  router.post('/activateAccount', auth.activateAccount);
  router.post('/recoverPasswordInit', auth.recoverPasswordInit);
  router.post('/recoverPasswordEnd', auth.recoverPasswordEnd);
  router.get('/logout', auth.logout);
  router.get('/verifyUsername', auth.verifyUsername);
  router.get('/verifyEmail', auth.verifyEmail);

  app.use('/api/auth', router);
};
