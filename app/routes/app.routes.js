const { authJwt } = require('../middleware');

module.exports = (app) => {
  app.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'token, Origin, Content-Type, Accept'
    );
    next();
  });

  const apps = require('../controllers/app.controller');

  var router = require('express').Router();

  router.get('/get', [authJwt.verifyToken], apps.get);
  router.get('/getOne', [authJwt.verifyToken], apps.getOne);
  router.get('/filter', [authJwt.verifyToken], apps.filter);
  router.get('/download', apps.download);
  router.post('/create', [authJwt.verifyToken], apps.create);
  router.put('/update', [authJwt.verifyToken], apps.update);
  router.delete('/delete', [authJwt.verifyToken], apps.delete);

  app.use('/api', router);
};
