module.exports = (app) => {
  const resultController = require('../controllers/result.controller');

  var router = require('express').Router();

  router.get('/getResultsByPool' , resultController.getResultsByPool);
  router.get('/getResultsByPoolAndUser' , resultController.getResultsByPoolAndUser);
  router.get('/getResultsByPoolAndUserWithoutSort' , resultController.getResultsByPoolAndUserWithoutSort);
  router.get('/getResultsUserForClient' , resultController.getResultsUserForClient);
  router.get('/getEventsForUser' , resultController.getEventsForUser);
  router.get('/getRankingForPool' , resultController.getRankingForPool);
  router.post('/updateResultByUserAndPool' , resultController.updateResultByUserAndPool);

  app.use('/api/result', router);
};
