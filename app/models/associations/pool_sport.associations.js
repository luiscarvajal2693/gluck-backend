const associations = (db) => {

  db.q_pools.belongsTo(db.q_sport, {foreignKey: 'fk_sport'});
  db.q_pools.hasMany(db.q_pools_details, {foreignKey: 'fk_pools'});
  // db.q_pools_details.belongsTo(db.q_pools, {foreignKey: 'fk_pools'});
};

module.exports = associations;
