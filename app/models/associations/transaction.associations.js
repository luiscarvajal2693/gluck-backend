const associations = (db) => {

  db.q_transaction.belongsTo(db.q_user, {foreignKey: 'fk_q_user'});
  db.q_transaction.belongsTo(db.q_pools, {foreignKey: 'fk_q_pools'});
};

module.exports = associations;
