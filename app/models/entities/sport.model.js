module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'sport',
        {
            rowid: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: Sequelize.STRING,
            },
            descriptios: {
                type: Sequelize.STRING,
            },
            img: {
                type: Sequelize.STRING(60),
            },
            status: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
            },
            date_Create: {
                type: Sequelize.DATE(3),
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)'),
            },
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'q_sport',
        }
    );
};
