module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'match',
        {
            rowid: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'q_pools_details',
        }
    );
};
