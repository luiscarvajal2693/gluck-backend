module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'transaction',
        {
            rowid: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            username: {
                type: Sequelize.STRING(20),
            },
            amount: {
                type: Sequelize.DOUBLE,
            },
            coins: {
                type: Sequelize.DOUBLE,
            },
            date_Create: {
                type: Sequelize.DATE(3),
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)'),
            },
            description: {
                type: Sequelize.STRING,
            },
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'q_transaction',
        }
    );
};
