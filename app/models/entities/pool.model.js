module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'pool',
        {
            rowid: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: Sequelize.STRING(500),
            },
            quantity: {
                type: Sequelize.INTEGER,
            },
            color: {
                type: Sequelize.STRING(7),
            },
            penalty: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
            },
            status: {
                type: Sequelize.INTEGER,
                defaultValue: 1,
            },
            date_Create: {
                type: Sequelize.DATE(3),
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)'),
            },
            puntaje_empate: {
                type: Sequelize.STRING(60),
            },
            puntaje_ganar: {
                type: Sequelize.STRING(60),
            },
            puntaje_perder: {
                type: Sequelize.STRING(60),
            },
            puntaje_resultado: {
                type: Sequelize.STRING(60),
            },
            limit_user: {
                type: Sequelize.INTEGER,
            },
            rules: {
                type: Sequelize.STRING(20),
            },
            password: {
                type: Sequelize.STRING(20),
            },
            pool_type: {
                type: Sequelize.STRING(50),
            },
            league: {
                type: Sequelize.STRING(50),
            },
            tournament_type: {
                type: Sequelize.STRING(50),
            },
            groups: {
                type: Sequelize.INTEGER,
            },
            groupsTeam: {
                type: Sequelize.INTEGER,
            },
            amountInput: {
                type: Sequelize.DOUBLE,
            },
            coinsInput: {
                type: Sequelize.DOUBLE,
            },
            dateFinish: {
                type: Sequelize.STRING,
            },
            timeFinish: {
                type: Sequelize.STRING,
            },
            awardType: {
                type: Sequelize.STRING,
            },
            awardValue: {
                type: Sequelize.DOUBLE,
            },
            hot: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
            },

        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'q_pools',
        }
    );
};
