module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'session',
    {
      token: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      expires: {
        type: Sequelize.DATE,
        field: 'expires',
      },
      status: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      grants: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      freezeTableName: true,
      tableName: 'Session',
    }
  );
};
