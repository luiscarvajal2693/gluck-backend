module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'user',
        {
            rowid: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            username: {
                type: Sequelize.STRING(20),
            },
            name: {
                type: Sequelize.STRING,
            },
            lastname: {
                type: Sequelize.STRING(60),
            },
            email: {
                type: Sequelize.STRING(100),
            },
            password: {
                type: Sequelize.STRING(100),
            },
            phone: {
                type: Sequelize.STRING(20),
            },
            address: {
                type: Sequelize.STRING(500),
            },
            state: {
                type: Sequelize.STRING(100),
            },
            city: {
                type: Sequelize.STRING(100),
            },
            code: {
                type: Sequelize.STRING(20),
            },
            type: {
                type: Sequelize.INTEGER,
            },
            fk_sport: {
                type: Sequelize.STRING(20),
            },
            status: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
            },
            img: {
                type: Sequelize.STRING(60),
            },
            ranking: {
                type: Sequelize.INTEGER,
            },
            date_Access: {
                type: Sequelize.DATE,
            },
            date_Create: {
                type: Sequelize.DATE(3),
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)'),
            },
            contador: {
                type: Sequelize.INTEGER,
            },
            status_code: {
                type: Sequelize.STRING(60),
            },
            amount: {
                type: Sequelize.DOUBLE,
            },
            coins: {
                type: Sequelize.DOUBLE,
            },
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'q_user',
        }
    );
};
