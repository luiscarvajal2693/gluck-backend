const dbConfig = require('../config/db.config');
const fs = require('fs');
const path = require('path');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

fs.readdirSync(__dirname + '/entities').forEach((file) => {
  if (file !== path.basename(__filename) && file.endsWith('.model.js')) {
    let model = require('./entities/' + file.replace(/\.js$/, ''))(
      sequelize,
      Sequelize
    );
    db[model.tableName] = model;
  }
});


fs.readdirSync(__dirname + '/associations').forEach((file) => {
  if (file !== path.basename(__filename) && file.endsWith('.associations.js')) {
    require('./associations/' + file.replace(/\.js$/, ''))(db);
  }
});

db.getOperator = function getOperator(type, Op) {
  if (type === 'INTEGER') {
    return Op.eq;
  }

  if (type.includes('VARCHAR')) {
    return Op.like;
  }
};

db.getValue = function getValue(type, value) {
  if (type === 'INTEGER') {
    return value;
  }

  if (type.includes('VARCHAR')) {
    return '%' + value + '%';
  }
};

module.exports = db;
