const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const Session = db.Session;

verifyToken = (req, res, next) => {
    let token = req.headers["token"];

    if (!token) {
        return res.status(200).send({
            code: "A403"
        });
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(200).send({
                code: "A401"
            });
        }
        req.userId = decoded.id;

        Session.findOne({
            where: {
                token: token,
                status: 'ACTIVE'
            },
        }).then((data) => {
            if (data) {
                next();
            } else {
                return res.status(200).send({
                    code: "A302"
                });
            }
        })
    });
};

const authJwt = {
    verifyToken: verifyToken,
};
module.exports = authJwt;
