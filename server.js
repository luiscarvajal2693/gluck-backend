const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const cronC = require('./app/controllers/cron');

const app = express();

const db = require('./app/models');
db.sequelize.sync();

require('dotenv').config();

var whitelist = process.env.WHITE_LIST_IPS.split(',');

var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

global.appRoot = path.resolve(__dirname);

// app.use(cors(corsOptions));
app.use(cors());

// app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit:50000}));

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.json({ message: 'Welcome to gluck application.' });
});

  require('./app/routes/app.routes')(app);
  // require('./app/routes/setting.routes')(app);
  // require('./app/routes/good.routes')(app);
require('./app/routes/file.routes')(app);
require('./app/routes/utilities.routes')(app);
require('./app/routes/result.routes')(app);
require('./app/routes/pool.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/auth.routes')(app);

cronC.startCronJob();

const PORT = process.env.SERVER_PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
