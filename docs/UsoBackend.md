# **Guía de uso del Proyecto Node JS Backend**

El siguiente instructivo especifica todos los pasos necesarios para configurar la aplicacion backend en un ambiente local.

## Paso 1 - Configurar las variables de entorno para la conexion con la base de datos:

```jsx
Se requiere crear un archivo .env a la altura de la raiz con la siguiente estructura de variables:

SERVER_PORT=
DB_HOST=
DB_USER=
DB_PASSWORD=
DB_SCHEMA=
DB_DIALECT=postgres
DB_POOL_MAX=5
DB_POOL_MIN=0
DB_POOL_ACQUIRE=30000
DB_POOL_IDLE=10000
EXPIRATION_TOKEN=86400
EXPIRATION_CRON_TIME=* 1 * * *

Donde:

SERVER_PORT -> puerto del servidor node js donde correra localmente
DB_HOST -> direccion host de la base de datos
DB_USER -> usuario de la base de datos
DB_PASSWORD -> clave de la base de datos
DB_SCHEMA -> esquema de la base de datos

EXPIRATION_TOKEN -> tiempo en segundos para determinar la expiracion de los tokens
EXPIRATION_CRON_TIME -> tiempo en formato crontab para correr el cron de expiracion de tokens

Parametros por defecto:
DB_DIALECT=postgres
DB_POOL_MAX=5
DB_POOL_MIN=0
DB_POOL_ACQUIRE=30000
DB_POOL_IDLE=10000
```
## Paso 2 - Instalar las dependencias:

```jsx
npm intall
```

## Paso 3 - ejecutar el backend: 

```jsx
npm run start
```

## Paso 4 - Probar los servicios expuestos

```jsx
Para ello nos apoyaremos de un archivo json en la misma carpeta que debemos importar en postman para cargar el set predeterminado que contiene la documentacion de los servicios realizados
```


## Codigos de Error para estandarizar las respuestas

```jsx
CodeType + Code     Description

A200                login Success or logout Success
A404                User Not Found
A401                Invalid Password or Unauthorized!
A403                No Token Provided
A302                Session Expired

D200                Success
D500                Internal Server Error
```


## para poder dedodificar los jsons que viajan en el filter usar el siguiente link

```jsx
https://jsonformatter.org/json-url-decode
```


## para poder dodificar los jsons que viajan en el filter usar el siguiente link

```jsx
https://jsonformatter.org/json-url-encode
```
